/**
 * Test Executive
 * ---------------------
 * Test Code Publisher Functionality
 *
 * FileName     : Main.cpp
 * Author       : Sahil Gupta
 * Date         : 25 March 2017
 * Version      : 1.0
 *
 * Usage:
 * 	TestExecutive /d DirPath Ext
 * 		DirPath - Directory containing source code files
 * 		Ext - Extension of source code files to proecess. Can be *.h and/or *.cpp
 * 	TestExecutive /f HtmFilePath
 * 		HtmFilePath - One or more html file path to open
 */

#include <Windows.h>
#include <iostream>

#include "..\..\FileSystem\FileSystem.h"
#include "..\..\Analyzer\Executive.h"
#include "..\..\DependencyAnalysis\TypeAnal\TypeAnal.h"
#include "..\..\DependencyAnalysis\DepAnal\DepAnal.h"
#include "..\..\DependencyAnalysis\Display\Display.h"
#include "..\Publisher\Publisher.h"

using namespace std;
using namespace CodeAnalysis;

void displayHelp()
{
    cout << endl
         << "Generates web pages to display source code files"
         << endl
         << "TestExecutive /d DirPath Ext"
         << "DirPath - Directory containing source code files"
         << "Ext - Extension of source code files to proecess. Can be *.h and/or *.cpp"
         << endl
         << "TestExecutive /f FilePath"
         << "FilePath - One or more file path to open";
}

void getAbsolutePath(LPCSTR fileSpec, LPSTR buffer)
{
    const int BufSize = MAX_PATH;
    char filebuffer[BufSize];  // don't use but GetFullPathName will
    char* name = filebuffer;
    ::GetFullPathNameA(fileSpec, BufSize, buffer, &name);
}

void OpenPublishedCode(const char *file)
{
    char fileAbsPath[MAX_PATH];
    getAbsolutePath(file, fileAbsPath);
    ShellExecute(NULL, "open", fileAbsPath, NULL, NULL, SW_SHOWNORMAL);
}

void PublishCode(int argc, char* argv[])
{
    Display d("Type_Based_Dependency_Analysis");

    /* Code Analysis */
    cout << "\nAnalyzing Source Code Files...";
    CodeAnalysisExecutive c;
    c.DoCodeAnal(argc, argv);

    /* Type Analysis */
    TypeAnal ta;
    ta.doTypeAnal();
    d.addNewComponent(ta.GetRootElement());
    d.displayTypeTable(ta.GetTypeTable());

    /* Dependency Analysis */
    cout << "\nDoing Dependency Analysis...";
    DepAnal da(ta.GetTypeTable());
    da.setFiles(c.getAllFiles());
    da.doDepAnal();
    da.dispalyDepAnal();
    d.addNewComponent(da.GetRootElement());

    /* Code Publisher */
    Publisher p;
    p.setFiles(da.getFiles());
    p.setDependencyAnalysis(da.getDepTable());
    p.GenerateHtmPage();
    string IndexFile = p.generateIndexFile(argv[1]);
    OpenPublishedCode(IndexFile.c_str());
}

int main(int argc, char* argv[])
{
    if (argc < 2)
    {
        cout << "Invalid Parameters";
        displayHelp();
        return 0;
    }

    switch (argv[1][1])
    {
        case 'd':
            PublishCode(argc - 1, argv + 1);
            break;

        case 'f':
            for (int i = 2; i < argc; i++) {
                OpenPublishedCode(argv[i]);
            }
            break;

        default:
            cout << "Invalid Parameters";
            displayHelp();
            break;
    }
}