#pragma once
/**
 * Publisher
 * ---------------------
 * Generates Html page displaying source code
 *
 * FileName     : Publisher.h
 * Author       : Sahil Gupta
 * Date         : 28 March 2017
 * Version      : 1.0
 *
 * Package Operations:
 * -------------
 * Parses input files to generate .htm file corresponding to each source file
 * The generated file is a web page displaying source code in a web browser
 * Each source file contains hyperlink to dependent files.
 *
 * Public Interface
 * ----------------
 * Publisher() - Create new publisher instance
 * setFiles(const std::vector<FileType> files) - Pass list of files to publish
 * setDependencyAnalysis(const StringDb& DepAnalTable) - Set dependency analysis info
 * GenerateHtmPage() - Start html page generation
 * generateIndexFile(string Dir) - generate index file containing link to generated web pages
 *
 * Required files
 * -------------
 * ActionsAndRules.h, DbMain.h
 *
 * Build Process
 * -------------
 * devenv.exe CodeAnalyzerEx.sln /rebuild release
 *
 * Maintenance History
 * -------------------
 * ver 1.0 : 28 March 2017
 *  - first release
 */

#include <string>
#include <vector>
#include <map>

#include "../../Parser/ActionsAndRules.h"
#include "../../NoSqlDb/DbEngine/DbMain.h"

using namespace std;
using namespace CodeAnalysis;

class Publisher
{
public:
    using FileType = string;
    using SqlDbType = string;
    using StringDb = NoSqlDb<SqlDbType, SqlDbType>;
    using SizetPair = pair<size_t, size_t>;
    using SizetPairDb = NoSqlDb<SqlDbType, SizetPair>;
    using SizetPairRec = Record<SqlDbType, SizetPair >;

private:
    const string buttonId = "button";
    const string csJavaScriptFile = "PublisherJs.js";
    const string cssFilePath = "PublisherCss.css";

    vector<FileType> Files;
    const StringDb *DepAnal;
    AbstrSynTree& ASTref_;
    SizetPairDb ShowHideContents;

public:
    Publisher();
    void setFiles(const std::vector<FileType> files);
    void setDependencyAnalysis(const StringDb& DepAnalTable);
    void GenerateHtmPage();
    string generateIndexFile(string Dir);

private:
    string GenerateHtmFromSource(ifstream &in, const FileType& file);

    //Html
    string buildHtmlHeader();
    string buildHtmlBody(const FileType& file);
    string buildDependencySection(const FileType& file);
    string buildHtmlEnd();

    //Styling
    string addCssCode();
    string addCssFile(const string &filename);
    string addJavaScriptCode();
    string addJavaScriptFile(const string &filename);

    //Show Hide
    string addShowHideButton(const string& CodeBlockTag);
    string addShowHideStart(const string& CodeBlockTag);
    string addShowHideEnd();

    //AST
    void ParseAST(ASTNode* pNode);
    void AddToShowHideContentDb(string Name, string File, size_t startLine, size_t endLine);
    map<size_t, string> getShowHideLineMap(const SizetPairDb::Keys& keys, bool bIsStartTag);
    void displayShowHideContent(const SizetPairDb::Keys& keys);

    //Helper
    string GetFileName(const string& str);
    string getHtmlExt(const string& file);
    string getFilePath(const string& filename);
    void ReplaceCharWithStr(string &str, char char_to_find, const string str_to_replace);
    void ReplaceKey(map<size_t, string>& MyMap, const size_t& OldKey, const size_t& NewKey);
    string GetExePath();
};