/**
 * Publisher
 * ---------------------
 * Generates Html page displaying source code
 *
 * FileName     : Publisher.cpp
 * Author       : Sahil Gupta
 * Date         : 28 March 2017
 * Version      : 1.0
 *
 * @todo
 *   Bug: Dependency Analyzer does not handle namespace resolution
 *   Bug: Dependency Analyzer does not handle template parameters
 *   Bug: Dependency Analyzer does not handle multiple "main" functions
 *   Enhancement : Process #include in Dependency Analyzer
 *
 *   Enhancement : Create new folder for .htm files
 *   Enhancement : For braces on new line move collapse box to function line
 *
 *   Improvement: Better way to add relative file path? (See : buildHtmlHeader)
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <map>

#include "Publisher.h"
#include "..\..\Tokenizer\Tokenizer.h"
#include "..\..\FileSystem\FileSystem.h"

using namespace std;
using namespace Scanner;

Publisher::Publisher() : 
    DepAnal(nullptr), 
    ASTref_(Repository::getInstance()->AST())
{

}

void Publisher::setFiles(const vector<FileType> files)
{
    Files = files;
}

void Publisher::setDependencyAnalysis(const Publisher::StringDb& DepAnalTable)
{
    DepAnal = &DepAnalTable;
}

void Publisher::GenerateHtmPage()
{
    ASTNode* pRoot = ASTref_.root();
    ParseAST(pRoot);
    //ShowHideContents.display();
    //displayShowHideContent(ShowHideContents.GetKeys());

    cout << "\n\nPublishing Code...";

    for (auto file : Files)
    {
        string fpath = FileSystem::Path::getFullFileSpec(file);
        ifstream in(fpath);
        if (!in.good()) {
            cout << "\nCan't open " << file << "\n";
            continue;
        }

        cout << "\nProcessing file : " << file;
        ofstream out(getHtmlExt(file));  // Create Htm file
        out << GenerateHtmFromSource(in, file);
        out.close();

        in.close();
    }
}

string Publisher::generateIndexFile(string Dir)
{
    string fpath = FileSystem::Path::getFullFileSpec(Dir);
    string filename = FileSystem::Path::fileSpec(fpath, getHtmlExt("Index"));

    ofstream html(filename);

    html << buildHtmlHeader();

    html << "<body>\n";
    html << "<h3>" << "Index for Directory \"" + GetFileName(fpath) << "\"</h3>\n";
    html << "<hr/>\n";

    for (auto file : Files)
    {
        html << "\t<a href = \"" << getHtmlExt(file) << "\">" << GetFileName(file) << "</a>\n";
        html << "\t<br/>\n";
    }

    html << "</body>\n";
    html << "</html>\n";

    return filename;
}

/*
 * PRIVATE MEMBERS
 */

string Publisher::GenerateHtmFromSource(ifstream &in, const Publisher::FileType& file)
{
    string line;
    ostringstream out;

    SizetPairDb::Keys MyKeys = ShowHideContents.getCategoryPattern(GetFileName(file)); //Get all scopes for this file 
    map<size_t, string> &StartCollapseLines = getShowHideLineMap(MyKeys, true); //Start Scope
    map<size_t, string> &EndCollapseLines = getShowHideLineMap(MyKeys, false);  //End Scope

    out << buildHtmlHeader();
    out << buildHtmlBody(file);

    for (size_t LineCount = 1; getline(in, line); ++LineCount) {
        //Check for Start Scope
        if (StartCollapseLines.count(LineCount) > 0) {
            //A bug in AST causes incorrect line number.
            if (line.find('{') == string::npos) {
                getline(in, line);
                ReplaceKey(StartCollapseLines, LineCount, LineCount + 1);
                ++LineCount;
            }
            out << addShowHideButton(StartCollapseLines[LineCount]);
            // Check if line is of form "function {", if So print the line without "{" and put "{" on next line
            if (line.find(StartCollapseLines[LineCount]) != string::npos) {
                ReplaceCharWithStr(line, '<', "&lt;");
                ReplaceCharWithStr(line, '>', "&gt;");
                ReplaceCharWithStr(line, '{', " ");
                out << line << "\n";
                line = "{";
            }
            out << addShowHideStart(StartCollapseLines[LineCount]);
        }

        //Put Source Code Line
        ReplaceCharWithStr(line, '<', "&lt;");
        ReplaceCharWithStr(line, '>', "&gt;");
        out << line << "\n";

        //Check for End Scope
        if (EndCollapseLines.count(LineCount) > 0) {
            out << addShowHideEnd();
        }
    }

    out << buildHtmlEnd();
    return out.str();
}

string Publisher::buildHtmlHeader()
{
    ostringstream header;

    header << "<html>\n";
    header << "<head>\n";

    //Add the JavaScript function to toggle visibility
    string JsFilepath = FileSystem::Path::fileSpec(FileSystem::Path::getPath(GetExePath()), "..\\..\\Code-Publisher\\Publisher\\" + csJavaScriptFile);
    header << addJavaScriptFile(JsFilepath);
    //header << addJavaScriptCode();

    //Add CSS file for styling
    string CssFilepath = FileSystem::Path::fileSpec(FileSystem::Path::getPath(GetExePath()), "..\\..\\Code-Publisher\\Publisher\\" + cssFilePath);
    header << addCssFile(CssFilepath);
    //header << addCssCode();
    
    header << "</head>\n";

    return header.str();
}

string Publisher::buildHtmlBody(const Publisher::FileType& file)
{
    ostringstream body;

    body << "<body>\n";
    body << "<h3>" << file << "</h3>\n";
    body << "<hr/>\n";

    //Add Dependencies
    body << buildDependencySection(file);

    body << "<pre>\n";

    return body.str();
}

string Publisher::buildDependencySection(const Publisher::FileType& file)
{
    ostringstream depSection;

    if (nullptr == DepAnal)
        return "";

    depSection << "\t<div class = \"dependency\">\n";
    depSection << "\t<h4>Dependencies:</h4>\n";

    //Get all dependent childs for this file
    const StringDb::Keys DepChilds = (const_cast<StringDb *>(DepAnal))->getChild(GetFileName(file));

    for (SqlDbType DepChildFile : DepChilds)
    {
        depSection << "\t<a href = \"" << getHtmlExt(getFilePath(DepChildFile)) << "\">" << DepChildFile << "</a>\n";
        depSection << "\t<br/>\n";
    }

    depSection << "\t</div>\n";
    depSection << "<hr/>\n";

    return depSection.str();
}

string Publisher::buildHtmlEnd()
{
    ostringstream end;

    end << "\n</pre>\n";
    end << "</body>\n";
    end << "</html>\n";

    return end.str();
}

string Publisher::addCssCode()
{
    ostringstream css;

    css << "\t<style>\n";

    css << "\tbody{\n";
    css << "\t\tmargin: 20px;\n";
    css << "\t\tcolor: black;\n";
    css << "\t\tbackground-color: #eee;\n";
    css << "\t\tfont-family: Consolas;\n";
    css << "\t\tfont-weight: 600;\n";
    css << "\t\tfont-size: 110%;\n";
    css << "\t}\n";

    css << "\n";
    css << "\t.dependency{\n";
    css << "\t\tmargin-left: 20px;\n";
    css << "\t\tmargin-right: 20px;\n";
    css << "\t}\n";

    css << "\n";
    css << "\th4{\n";
    css << "\t\tmargin-bottom: 3px;\n";
    css << " \t\tmargin-top: 3px;\n";
    css << "\t}\n";
    css << "\t</style>\n";

    return css.str();
}

string Publisher::addCssFile(const string &filename)
{
    return "\t<link rel = \"stylesheet\" type = \"text/css\" href = \"" + filename + "\">\n";
}

string Publisher::addJavaScriptCode()
{
    ostringstream func;

    func << "\t<script>\n";
    func << "\tfunction showhide(divId) {\n";
    func << "\tvar x = document.getElementById(divId);\n";
    func << "\tif (x.style.display === 'none') {\n";
    func << "\t\tx.style.display = 'block';\n";
    func << "\t\tdocument.getElementById(\"" + buttonId + "\" + divId).value = \"-\";\n";
    func << "\t} else {\n";
    func << "\t\tx.style.display = 'none';\n";
    func << "\t\tdocument.getElementById(\"" + buttonId + "\" + divId).value = \"+\";\n";
    func << "\t}\n";
    func << "\t}\n";
    func << "\t</script>\n";

    return func.str();
}

string Publisher::addJavaScriptFile(const string &filename)
{
    return "\t<script src = \"" + filename + "\"></script>\n";
}

string Publisher::addShowHideButton(const string& CodeBlockTag)
{
    return "<input type=\"button\" onclick = \"showhide('" + CodeBlockTag + "')\" value = \"-\" id=\"" + buttonId + CodeBlockTag + "\"/>";
}

string Publisher::addShowHideStart(const string& CodeBlockTag)
{
    return "<div id = \"" + CodeBlockTag + "\">\n";
}

string Publisher::addShowHideEnd()
{
    return "</div>\n";
}

void Publisher::ParseAST(ASTNode* pNode)
{
    if (pNode->type_ == "function" || pNode->type_ == "class")
    {
        //cout << endl << pNode->name_ << "\t" << pNode->package_ << "\t[" << pNode->startLineCount_ << " ," << pNode->endLineCount_ << "]";
        AddToShowHideContentDb(pNode->name_, pNode->package_, pNode->startLineCount_, pNode->endLineCount_);
    }

    //Global Function
    if (pNode->type_ == "namespace")
    {
        for (auto pChild : pNode->children_)
        {
            if (pChild->type_ == "function")
            {
                //cout << endl << pChild->name_ << "\t" << pChild->package_ << "\t[" << pChild->startLineCount_ << " ,"<<pChild->endLineCount_ <<"]";
                AddToShowHideContentDb(pChild->name_, pChild->package_, pChild->startLineCount_, pChild->endLineCount_);
            }
        }
    }

    for (auto pChild : pNode->children_)
        ParseAST(pChild);
}

void Publisher::AddToShowHideContentDb(string Name, string File, size_t startLine, size_t endLine)
{
    if ((endLine - startLine) < 1)
        return;

    SizetPair LineCount(startLine, endLine);
    SizetPairRec DummyRec("", "", File, LineCount);
    ShowHideContents.addKey(Name, DummyRec);
}

map<size_t, string> Publisher::getShowHideLineMap(const SizetPairDb::Keys& keys, bool bIsStartTag)
{
    map<size_t, string> LineMap;

    for (SizetPairDb::Key MyKey : keys)
    {
        SizetPairRec* pVal = ShowHideContents.GetValue(MyKey);
        if(bIsStartTag)
            LineMap[pVal->GetData().first] = MyKey;
        else
            LineMap[pVal->GetData().second] = MyKey;
    }

    return LineMap;
}

void Publisher::displayShowHideContent(const SizetPairDb::Keys& keys)
{
    if (keys.empty())
        return;

    cout << "\nShow Hide Content Table";
    for (SizetPairDb::Key MyKey : keys)
    {
        SizetPairRec* pVal = ShowHideContents.GetValue(MyKey);
        if (nullptr == pVal)
        {
            cout << "\nInvalid Key (" << MyKey << ")";
        }
        else
        {
            ostringstream out;
            out.setf(ios::adjustfield, ios::left);

            out << "\n" 
                << setw(20) << MyKey
                << setw(15) << pVal->GetCategory() 
                << setw(10) << pVal->GetData().first 
                << setw(10) << pVal->GetData().second;

            cout << out.str();
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
//                           HELPER FUNCTION                                 //
///////////////////////////////////////////////////////////////////////////////

string Publisher::GetFileName(const string& str)
{
    size_t found = str.find_last_of("/\\");
    //cout << " path: " << str.substr(0, found) << '\n';
    //cout << " file: " << str.substr(found + 1) << '\n';
    return str.substr(found + 1);
}

string Publisher::getHtmlExt(const string& file)
{
    return file + ".htm";
}

string Publisher::getFilePath(const string& filename)
{
    for (auto file : Files) {
        if (GetFileName(file) == filename)
        {
            return file;
        }
    }

    return filename;
}

void Publisher::ReplaceCharWithStr(string &str, char char_to_find, const string str_to_replace)
{
    size_t found;

    while ((found = str.find(char_to_find)) != string::npos)
        str.replace(found, 1, str_to_replace);
}

void Publisher::ReplaceKey(map<size_t, string>& MyMap, const size_t& OldKey, const size_t& NewKey)
{
    string Value = MyMap.at(OldKey);
    MyMap.erase(OldKey);
    MyMap[NewKey] = Value;
}

string Publisher::GetExePath()
{
    char ownPth[MAX_PATH];

    // Will contain exe path
    HMODULE hModule = GetModuleHandle(NULL);
    if (hModule != NULL)
    {
        // When passing NULL to GetModuleHandle, it returns handle of exe itself
        GetModuleFileName(hModule, ownPth, (sizeof(ownPth)));
    }

    return string(ownPth);
}

#ifdef __TEST_PUBLISHER__
int main(int argc, char *argv[])
{
    Publisher p;
    vector<Publisher::FileType> Files;

    for (int i = 1; i < argc; ++i ) {
        Files.push_back(argv[i]);
    }

    p.setFiles(Files);
    p.GenerateHtmPage();
    p.generateIndexFile(argv[2]);
    return 0;
}
#endif