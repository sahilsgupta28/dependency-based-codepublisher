/**
 * Type Table
 * ---------------------
 * Define data-structure for type analysis
 *
 * FileName     : TypeTable.cpp
 * Author       : Sahil Gupta
 * Date         : 01 March 2017
 * Version      : 1.0
 */

#include <iostream>
#include "TypeTable.h"
#include "..\..\NoSqlDb\Persistence\Persist.h"

void TypeTable::addType(std::string Name, std::string Type, std::string File)
{
    StringRec R1("", "", Type, File);
    TypeTableDb.addKey(Name, R1);
}

void TypeTable::PersistTypeTable(std::string FileName)
{
    Persist<SqlDbType, SqlDbType> PersistStringDb(TypeTableDb, false, FileName);
    PersistStringDb.save();
}

void TypeTable::LoadTypeTable(std::string FileName)
{
    Persist<SqlDbType, SqlDbType> PersistStringDb(TypeTableDb, false, FileName);
    PersistStringDb.load();
}

Persist<TypeTable::SqlDbType, TypeTable::SqlDbType>::SPtr TypeTable::GetRootElement()
{
    Persist<SqlDbType, SqlDbType> PersistDb(TypeTableDb, false);
    return PersistDb.GetXMLRoot("TypeTable");
}

#ifdef __TEST_TYPETABLE__
int main()
{
    TypeTable types;

    std::cout << "Testing Type Table";
    types.addType("iTest", "Global", "file1.h");
    types.addType("iFunc", "Func", "file2.h");

    //types.displayTypeTable();
}
#endif