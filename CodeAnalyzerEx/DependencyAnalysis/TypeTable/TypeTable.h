#pragma once
/**
 * Type Table
 * ---------------------
 * Define data-structure for type analysis
 *
 * FileName     : TypeTable.h
 * Author       : Sahil Gupta
 * Date         : 01 March 2017
 * Version      : 1.0
 *
 * Package Operations:
 * -------------
 * Provides a container class that stores type information needed for dependency analysis.
 *
 * Public Interface
 * ----------------
 * addType(std::string Name, std::string Type, std::string File) - Add new type to type table
 * PersistTypeTable(std::string FileName) - Persist type table in files
 * LoadTypeTable(std::string FileName) - Load Type table from file
 * GetRootElement() - get XML element to display/persist type table
 * GetTypeTable() - get reference to type table 
 *
 * Required files
 * -------------
 * DbMain.h, Persist.h
 *
 * Build Process
 * -------------
 * devenv.exe CodeAnalyzerEx.sln /rebuild release
 *
 * Maintenance History
 * -------------------
 * ver 1.0 : 01 March 2017
 *  - first release
 */

#include "../../NoSqlDb/DbEngine/DbMain.h"
#include "..\..\NoSqlDb\Persistence\Persist.h"

class TypeTable
{
public:
    using SqlDbType = std::string;
    using StringDb = NoSqlDb<SqlDbType, SqlDbType>;
    using StringRec = Record<SqlDbType, SqlDbType>;

private:
    NoSqlDb<SqlDbType, SqlDbType> TypeTableDb;

public:
    void addType(std::string Name, std::string Type, std::string File);
    void PersistTypeTable(std::string FileName);
    void LoadTypeTable(std::string FileName);
    Persist<SqlDbType, SqlDbType>::SPtr GetRootElement();
    StringDb& GetTypeTable() { return TypeTableDb; }
};