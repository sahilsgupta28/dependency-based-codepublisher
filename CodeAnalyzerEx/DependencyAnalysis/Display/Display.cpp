/**
 * Display
 * ---------------------
 * Builds XML display to persist, retrieve and display Dependency Analyzer components
 *
 * FileName     : Display.cpp
 * Author       : Sahil Gupta
 * Date         : 01 March 2017
 * Version      : 1.0
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>

#include "Display.h"

#include "../../NoSqlDb/XmlInterface/XmlDocument.h"

Display::Display(string ModuleName)
{
    pRoot = makeTaggedElement(ModuleName);
}

void Display::save(std::string FilePath)
{
    XmlDocument doc;
    doc.docElement() = pRoot;

    ofstream out(FilePath);
    out << doc.toString();
    out.close();
}

void Display::addNewComponent(Display::SPtr pNode)
{
    pRoot->addChild(pNode);
}

std::string Display::ToXmlString()
{
    XmlDocument doc;

    doc.docElement() = pRoot;

    return doc.toString();
}

void Display::displayTypeTable(NoSqlDb<std::string, std::string> &db)
{
    std::cout << "\nType Table ::";
    std::cout << "\n-------------------------------------------------------";
    std::ostringstream out;
    out.setf(std::ios::adjustfield, std::ios::left);
    out << "\n"
        << std::setw(25) << "TypeName";
    out << std::setw(16) << "File";
    out << std::setw(16) << "Type";
    std::cout << out.str();
    std::cout << "\n-------------------------------------------------------";
    formatTypeTable(db);
    std::cout << "\n-------------------------------------------------------\n";
}

void Display::formatTypeTable(NoSqlDb<std::string, std::string> &TypeTableDb)
{
    const NoSqlDb<std::string, std::string>::Keys MyKeys = TypeTableDb.GetKeys();

    for (auto MyKey : MyKeys)
    {
        Record<std::string, std::string>* pVal = TypeTableDb.GetValue(MyKey);
        std::string KeyString = Convert<std::string>::toString(MyKey);
        std::ostringstream out;
        out.setf(std::ios::adjustfield, std::ios::left);
        out << "\n" << std::setw(25) << KeyString;
        out << std::setw(16) << pVal->GetData();
        out << std::setw(16) << pVal->GetCategory();
        std::cout << out.str();
    }
}

#ifdef TEST_DISPLAY_
int main()
{
    Display display("TestXml");

    Display::SPtr pR1 = makeTaggedElement("Root1");

    Display::SPtr pNameElem1 = makeTaggedElement("Name");
    pNameElem1->addChild(makeTextElement("Jon"));
    pR1->addChild(pNameElem1);

    Display::SPtr pNameElem2 = makeTaggedElement("Surname");
    pNameElem2->addChild(makeTextElement("Doe"));
    pR1->addChild(pNameElem2);

    display.addNewComponent(pR1);

    Display::SPtr pR2 = makeTaggedElement("Root2");

    Display::SPtr pNameElem3 = makeTaggedElement("Name2");
    pNameElem3->addChild(makeTextElement("Jon2"));
    pR2->addChild(pNameElem3);

    Display::SPtr pNameElem4 = makeTaggedElement("Surname2");
    pNameElem4->addChild(makeTextElement("Doe2"));
    pR2->addChild(pNameElem4);

    display.addNewComponent(pR2);

    display.save("TestXml.xml");
}
#endif