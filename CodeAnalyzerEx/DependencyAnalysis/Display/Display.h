#pragma once
/**
 * Display
 * ---------------------
 * Builds XML display to persist, retrieve and display Dependency Analyzer components
 *
 * FileName     : Display.h
 * Author       : Sahil Gupta
 * Date         : 01 March 2017
 * Version      : 1.0
 *
 * Package Operations:
 * -------------
 * Uses XML Library to build XML for Type Table, Dependency Table and Strong Components
 *
 * Public Interface
 * ----------------
 * Display(string ModuleName)
 * save(std::string FilePath)
 * addNewComponent(SPtr pNode)
 * ToXmlString()
 * displayTypeTable(NoSqlDb<std::string, std::string> &db)
 * formatTypeTable(NoSqlDb<std::string, std::string> &TypeTableDb)
 *
 * Required files
 * -------------
 * DbMain.h, XmlElement.h
 *
 * Build Process
 * -------------
 * devenv.exe CodeAnalyzerEx.sln /rebuild release
 *
 * Maintenance History
 * -------------------
 * ver 1.0 : 01 March 2017
 *  - first release
 */

#include "../../NoSqlDb/XmlInterface/XmlElement.h"
#include "../../NoSqlDb/DbEngine/DbMain.h"

using namespace std;
using namespace XmlProcessing;

class Display
{
public:
    using Xml = string;
    using SPtr = shared_ptr<AbstractXmlElement>;

private:
    SPtr pRoot;

public:
    Display(string ModuleName);
    void save(std::string FilePath);
    void addNewComponent(SPtr pNode);
    std::string ToXmlString();
    void displayTypeTable(NoSqlDb<std::string, std::string> &db);
    void formatTypeTable(NoSqlDb<std::string, std::string> &TypeTableDb);
};
