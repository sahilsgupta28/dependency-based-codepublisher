#pragma once
/**
 *  Graph
 *  ---------------------
 *  Builds a Graph data structure and analyzes strongly connected components in graph
 * 
 *  FileName     : Graph.h
 *  Author       : Sahil Gupta
 *  Date         : 01 March 2017
 *  Version      : 1.0
 * 
 *  Package Operations:
 *  -------------
 *  Graph Class - Provides methods to build directed graph
 *  SCC Class   - Provides methods to detect strongly connected components in graph using Tarjans Algorithm
 * 
 *  Public Interface
 *  ----------------
 *  GRAPH CLASS
 *
 *  Graph(int V)
 *  ~Graph()
 *  addEdge(int v, int w) - function to add an edge to graph
 *  addEdge(std::string v1, std::string v2)
 *  displayMap()
 * 
 *  SCC CLASS
 *
 *  SSC(int V)
 *  ~SSC()
 *  doSCC()
 *  showSsc();
 *  getSscList()
 *  getMap()
 *
 *  Required files
 *  -------------
 *  None
 * 
 *  Build Process
 *  -------------
 *  devenv.exe CodeAnalyzerEx.sln /rebuild release
 * 
 *  Maintenance History
 *  -------------------
 *  ver 1.0 : 01 March 2017
 *   - first release
 */

#include <list>
#include <stack>
#include <vector>
#include <unordered_map>

// A class that represents an directed graph
class Graph
{
protected:
    int Index;
    int V;                  // No. of vertices
    std::list<int> *adj;    // A dynamic array of adjacency lists
    std::unordered_map<std::string, int> Map;
    std::unordered_map<int,std::string> ReverseMap;
public:
    Graph(int V);   // Constructor
    virtual ~Graph();
    virtual void addEdge(int v, int w);   // function to add an edge to graph
    virtual void addEdge(std::string v1, std::string v2);
    void displayMap();
};

class SSC : public Graph
{
public:
    using List = std::list<std::vector<int>>;
private:
    List SscList;
    const int NIL = -1;

    /**  A recursive function that finds and prints strongly connected
     *  components using DFS traversal
     *  u : The vertex to be visited next
     *  disc[] : Stores discovery times of visited vertices
     *  low[] : earliest visited vertex (the vertex with minimum
     *            discovery time) that can be reached from subtree
     *            rooted with current vertex
     *  *st : To store all the connected ancestors (could be part of SCC)
     *  stackMember[] : bit/index array for faster check whether a node is in stack
     */
    void SCCUtil(int u, int disc[], int low[], std::stack<int> *st, bool stackMember[]);

public:
    SSC(int V);
    ~SSC();

    void doSCC();    // prints strongly connected components
    void showSsc();
    List getSscList() { return SscList; }
    std::unordered_map<int, std::string>& getMap() { return ReverseMap; };
};

