/**
 *  Graph
 *  ---------------------
 *  Builds a Graph data structure and analyzes strongly connected components in graph
 *
 *  FileName     : Graph.cpp
 *  Author       : Sahil Gupta
 *  Date         : 01 March 2017
 *  Version      : 1.0
 */

#include <iostream>
#include <list>
#include <stack>
#include <algorithm>
#include "Graph.h"

using namespace std;

Graph::Graph(int V)
{
    this->V = V;
    adj = new list<int>[V];
    Index = 0;
}

Graph::~Graph()
{
    delete[] adj;
}

void Graph::addEdge(int v, int w)
{
    //if(v>V) throw
    adj[v].push_back(w);
}

void Graph::addEdge(std::string v1, std::string v2)
{
    int iv1, iv2;

    if (Map.find(v1) == Map.end())
    {
        iv1 = Index;
        Map.insert(make_pair(v1, iv1));
        ReverseMap.insert(make_pair(iv1, v1));
        Index++;
    }
    else
        iv1 = Map[v1];

    if (Map.find(v2) == Map.end())
    {
        iv2 = Index;
        Map.insert(make_pair(v2, iv2));
        ReverseMap.insert(make_pair(iv2, v2));
        Index++;
    }
    else
        iv2 = Map[v2];

    addEdge(iv1, iv2);
}

void Graph::displayMap()
{
    for (auto itr = Map.begin(); itr != Map.end(); itr++)
    {
        cout << itr->first.c_str() << "  " << itr->second << endl;
    }
}

SSC::SSC(int V) : Graph(V)
{
    //SscList = new list<int>[V];
}

SSC::~SSC()
{
    //delete[] SscList;
}

void SSC::SCCUtil(int u, int disc[], int low[], stack<int> *st, bool stackMember[])
{
    static int time = 0;
    disc[u] = low[u] = ++time;
    st->push(u);
    stackMember[u] = true;

    list<int>::iterator i; // Go through all vertices adjacent to this
    for (i = adj[u].begin(); i != adj[u].end(); ++i) {
        int v = *i;  // v is current adjacent of 'u'
        if (disc[v] == -1) // If v is not visited yet, then recur for it
        {
            SCCUtil(v, disc, low, st, stackMember);
            // Check if the subtree rooted with 'v' has a
            // connection to one of the ancestors of 'u'
            // Case 1 (per above discussion on Disc and Low value)
            low[u] = min(low[u], low[v]);
        }
        // Update low value of 'u' only of 'v' is still in stack
        // (i.e. it's a back edge, not cross edge).
        // Case 2 (per above discussion on Disc and Low value)
        else if (stackMember[v] == true)
            low[u] = min(low[u], disc[v]);
    }

    // head node found, pop the stack and print an SCC
    int w = 0;  // To store stack extracted vertices
    if (low[u] == disc[u]){
        vector<int> vecw;
        while (st->top() != u) {
            w = (int)st->top();
            //cout << w << " ";
            vecw.push_back(w);
            stackMember[w] = false;
            st->pop();
        }
        w = (int)st->top();
        //cout << w << "\n";
        vecw.push_back(w);
        SscList.push_back(vecw);
        stackMember[w] = false;
        st->pop();
    }
}

// The function to do DFS traversal. It uses SCCUtil()
void SSC::doSCC()
{
    int *disc = new int[V];
    int *low = new int[V];
    bool *stackMember = new bool[V];
    stack<int> *st = new stack<int>();

    // Initialize disc and low, and stackMember arrays
    for (int i = 0; i < V; i++)
    {
        disc[i] = NIL;
        low[i] = NIL;
        stackMember[i] = false;
    }

    // Call the recursive helper function to find strongly
    // connected components in DFS tree with vertex 'i'
    for (int i = 0; i < V; i++)
        if (disc[i] == NIL)
            SCCUtil(i, disc, low, st, stackMember);
}

void SSC::showSsc()
{
    int i = 0;
    cout << "\n\nStrongly Connected Components :: \n";
    for (List::const_iterator ci = SscList.begin(); ci != SscList.end(); ++ci)
    {
        cout << "[" << ++i << "] ";
        for (vector<int>::const_iterator cv = (*ci).begin(); cv != (*ci).end(); ++cv)
            if (!ReverseMap.empty())
                cout << ReverseMap[*cv].c_str() << " ";
            else
                cout << *cv << " ";
        cout << endl;
    }
}

void TestIntegerMap()
{
    SSC g1(5); 
    g1.addEdge(1, 0); g1.addEdge(0, 2); g1.addEdge(2, 1);
    g1.addEdge(0, 3);
    g1.addEdge(3, 4);
    g1.doSCC();
    g1.showSsc();

    SSC g2(4);
    g2.addEdge(0, 1);
    g2.addEdge(1, 2);
    g2.addEdge(2, 3);
    g2.doSCC();
    g2.showSsc();

    SSC g3(7);
    g3.addEdge(0, 1); g3.addEdge(1, 2); g3.addEdge(2, 0);
    g3.addEdge(1, 3);
    g3.addEdge(1, 4);
    g3.addEdge(1, 6);
    g3.addEdge(3, 5);
    g3.addEdge(4, 5);
    g3.doSCC();
    g3.showSsc();

    SSC g4(10);
    g4.addEdge(0, 1); g4.addEdge(0, 3);
    g4.addEdge(1, 2); g4.addEdge(1, 4);
    g4.addEdge(2, 0); g4.addEdge(2, 6);
    g4.addEdge(3, 2);
    g4.addEdge(4, 5); g4.addEdge(4, 6);
    g4.addEdge(5, 6); g4.addEdge(5, 7); g4.addEdge(5, 8); g4.addEdge(5, 9);
    g4.addEdge(6, 4);
    g4.addEdge(7, 9);
    g4.addEdge(8, 9);
    g4.addEdge(9, 8);
    g4.doSCC();
    g4.showSsc();

    SSC g5(5);
    g5.addEdge(0, 1); g5.addEdge(1, 2); g5.addEdge(2, 3); g5.addEdge(2, 4); g5.addEdge(3, 0); g5.addEdge(4, 2);
    g5.doSCC();
    g5.showSsc();
}

void TestStringMap()
{
    SSC g1(5);
    g1.addEdge("1", "0"); g1.addEdge("0", "2"); g1.addEdge("2", "1");
    g1.addEdge("0", "3");
    g1.addEdge("3", "4");
    cout << "\nMap in first graph \n"; g1.displayMap();
    g1.doSCC();
    g1.showSsc();

    SSC g2(4);
    g2.addEdge("00", "01");
    g2.addEdge("01", "02");
    g2.addEdge("02", "03");
    cout << "\nMap in second graph \n"; g2.displayMap();
    g2.doSCC();
    g2.showSsc();

    SSC g4(10);
    g4.addEdge("val-0", "val-1"); g4.addEdge("val-0", "val-3");
    g4.addEdge("val-1", "val-2"); g4.addEdge("val-1", "val-4");
    g4.addEdge("val-2", "val-0"); g4.addEdge("val-2", "val-6");
    g4.addEdge("val-3", "val-2");
    g4.addEdge("val-4", "val-5"); g4.addEdge("val-4", "val-6");
    g4.addEdge("val-5", "val-6"); g4.addEdge("val-5", "val-7"); g4.addEdge("val-5", "val-8"); g4.addEdge("val-5", "val-9");
    g4.addEdge("val-6", "val-4");
    g4.addEdge("val-7", "val-9");
    g4.addEdge("val-8", "val-9");
    g4.addEdge("val-9", "val-8");
    cout << "\nMap in fourth graph \n"; g4.displayMap();
    g4.doSCC();
    g4.showSsc();

    SSC g5(5);
    g5.addEdge("QAZ0", "QAZ1"); g5.addEdge("QAZ1", "QAZ2"); \
    g5.addEdge("QAZ2", "QAZ3"); g5.addEdge("QAZ2", "QAZ4"); \
    g5.addEdge("QAZ3", "QAZ0"); g5.addEdge("QAZ4", "QAZ2");
    cout << "\nMap in fifth graph \n"; g5.displayMap();
    g5.doSCC();
    g5.showSsc();
}

#ifdef __TEST_GRAPH__

// Driver program to test above function
int main()
{
    TestIntegerMap();
    TestStringMap();
    return 0;
}

#endif // __TEST_GRAPH__