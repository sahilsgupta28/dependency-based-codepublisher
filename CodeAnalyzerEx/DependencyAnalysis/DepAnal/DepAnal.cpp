/**
 * Dependency Analyzer
 * ---------------------
 * Generates dependency analysis beween source files
 *
 * FileName     : DepAnal.cpp
 * Author       : Sahil Gupta
 * Date         : 01 March 2017
 * Version      : 1.0
 */

#include "DepAnal.h"
#include "..\..\Tokenizer\Tokenizer.h"
#include "..\..\Analyzer\Executive.h"
#include "..\TypeAnal\TypeAnal.h"
#include "..\..\NoSqlDb\Persistence\Persist.h"

namespace CodeAnalysis
{
    using namespace Scanner;

DepAnal::DepAnal(NoSqlDb<std::string, std::string>& aTypeTable) :
    ASTref_(Repository::getInstance()->AST()),
    scopeStack_(Repository::getInstance()->scopeStack()),
    toker_(*(Repository::getInstance()->Toker())),
    TypeTable_(aTypeTable)
{

}

void DepAnal::setFiles(const std::vector<FileType> files)
{
    Files = files;
}

const std::vector<DepAnal::FileType>& DepAnal::getFiles()
{
    return Files;
}


//todo: use same function as in TypeAnal
bool doDisplayForDepTable(ASTNode* pNode)
{
    //function - can have object creation in functions
    //namespace - can have global declaration of classes in other classes
    //class, struct - can have object creation in these
    static std::string toDisplay[] = {
        "function", "namespace", "class", "struct"
    };
    for (std::string type : toDisplay)
    {
        if (pNode->type_ == type)
            return true;
    }
    return false;
}

//void DepAnal::generateDepAnalTable(ASTNode* pNode)
//{
//    StringDb::Keys MyKeys;
//
//    MyKeys = TypeTable_.GetKeys();
//    for (std::string key : MyKeys)
//    {
//        StringRec* pRec = TypeTable_.GetValue(key);
//        StringRec DummyRec("", "", "", "");
//        DepAnalTable.addKey(pRec->GetData(), DummyRec);
//    }
//}

std::string SplitFilename(const std::string& str)
{
    std::size_t found = str.find_last_of("/\\");
    //std::cout << " path: " << str.substr(0, found) << '\n';
    //std::cout << " file: " << str.substr(found + 1) << '\n';
    return str.substr(found + 1);
}

void DepAnal::ParseTokens()
{
    for (auto file : Files)
    {
        std::ifstream in(file);
        if (!in.good())
        {
            std::cout << "\nCan't open " << file<< "\n";
            return;
        }
        cout << "\nProcessing file : " << file;
        Toker toker;
        toker.returnComments(false);
        toker.attach(&in);
        StringDb::Keys Keys = TypeTable_.GetKeys();
        do
        {
            std::string tok = toker.getTok();
            for (auto Key : Keys) {
                if (Key == tok) {
                    addToDepTable(SplitFilename(file), TypeTable_.GetValue(Key)->GetData());
                }
            }
        } while (in.good());
        in.close();
    }
}

// Handle
// 1. Class Member Variables
// 2. Member Declarations in Functions
// 3. Global Data usage
// 4. Function definition in .cpp from .h
// 5. Test initialization list global variable initialization

// Detect use of variables in functions [global]
// This doesnt come up as data declaration or statement
// eg: convert.h : function(id) : conv_common = g_common;
// eg: datetime.cpp : GetCurrentDateTime() : g_common;

void DepAnal::fillDepTable(ASTNode* pNode)
{
    static StringDb::Keys AllTypes = TypeTable_.GetKeys();

    if (doDisplayForDepTable(pNode))
    {
        //Link .cpp and .h
        if (pNode->type_ == "class" || pNode->type_ == "stuct") 
        {
            for (auto pChild : pNode->children_)
            {
                if (pChild->type_ == "function")
                {
                    if (pChild->package_ != pNode->package_)
                    {
                        addToDepTable(pChild->package_, pNode->package_);
                        addToDepTable(pNode->package_, pChild->package_);
                    }
                }
            }
        }

        // Detect object creation of types in functions [class | struct | enum | using]
        for (auto pDecl : pNode->decl_) 
        {
            if (pDecl.declType_ != dataDecl) {
                continue;
            }

            for (auto Key : AllTypes) {
                Scanner::ITokCollection* pTc = pDecl.pTc;
                if (pTc->find(Key) < pTc->length())
                {
                    addToDepTable(pDecl.package_, TypeTable_.GetValue(Key)->GetData());
                }
            }
        }
    }
    for (auto pChild : pNode->children_)
        fillDepTable(pChild);
}

void DepAnal::addToDepTable(const std::string& Parent, const std::string &Child)
{

    /* If its the same file, skip */
    if (Parent == Child) {
        return;
    }

    StringRec DummyRec("", "", "", "");
    DepAnalTable.addKey(Parent, DummyRec);
    DepAnalTable.addKey(Child, DummyRec);

    DepAnalTable.addChild(Parent, Child);
}

void DepAnal::dispalyDepAnal()
{
    std::cout << "\n\nDependency Table ::";
    std::cout << "\n-------------------------------------------------------";
    std::ostringstream out;
    out.setf(std::ios::adjustfield, std::ios::left);
    out << "\n"
        << std::setw(25) << "FileName";
    out << std::setw(16) << "FileName";
    std::cout << out.str();
    std::cout << "\n-------------------------------------------------------";
    formatDepTable();
    std::cout << "\n-------------------------------------------------------";
}

DepAnal::StringDb& DepAnal::getDepTable()
{
    return DepAnalTable;
}

void DepAnal::formatDepTable()
{
    const StringDb::Keys MyKeys = DepAnalTable.GetKeys();

    for (SqlDbType MyKey : MyKeys)
    {
        std::string KeyString = Convert<SqlDbType>::toString(MyKey);
        std::ostringstream out;
        out.setf(std::ios::adjustfield, std::ios::left);
        out << "\n" << std::setw(25) << KeyString;
        const StringDb::Keys ChildKeys = DepAnalTable.getChild(MyKey);
        
        std::string Childs = "[ ";
        for (SqlDbType ChildKey : ChildKeys)
        {
            //todo: optimize using string builder
            Childs += ChildKey + ", ";
        }
        //Childs[Childs.length() - 2] = ' ';
        out << Childs << "]";
        std::cout << out.str();
    }
}

void DepAnal::doDepAnal()
{
    ASTNode* pRoot = ASTref_.root();

    //generateDepAnalTable(pRoot);
    fillDepTable(pRoot);
    ParseTokens();
    //dispalyDepAnal();
}

Persist<TypeTable::SqlDbType, TypeTable::SqlDbType>::SPtr DepAnal::GetRootElement()
{
    Persist<SqlDbType, SqlDbType> PersistDb(DepAnalTable, false);
    return PersistDb.GetXMLRoot("DependencyAnalysis");
}

}

#ifdef _TEST_DEPANAL_
int main(int argc, char* argv[])
{
    using namespace CodeAnalysis;

    CodeAnalysisExecutive c;
    c.DoCodeAnal(argc, argv);

    TypeAnal ta;
    ta.doTypeAnal();

    DepAnal da(ta.GetTypeTable());
    da.setFiles(c.getAllFiles());
    da.doDepAnal();
}
#endif