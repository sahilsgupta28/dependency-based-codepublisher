#pragma once
/**
 * Dependency Analyzer
 * ---------------------
 * Generates dependency analysis beween source files
 *
 * FileName     : DepAnal.h
 * Author       : Sahil Gupta
 * Date         : 01 March 2017
 * Version      : 1.0
 *
 * Package Operations:
 * -------------
 * Analyzes dependecny between source files using Type Table and generates dependency Table
 *
 * Public Interface
 * ----------------
 * DepAnal(NoSqlDb<std::string, std::string>& aTypeTable) - create new instance using type table
 * doDepAnal() - start dependency analysis
 * dispalyDepAnal() - display dependency table
 * getDepTable() - return reference to dependency table
 * setFiles(const std::vector<FileType> files) - set files to analyze
 * getFiles() - return reference to files
 * GetRootElement() - get reference to XML element to persist
 *
 * Required files
 * -------------
 * DbMain.h, ActionAndRules.h, Persist,h
 *
 * Build Process
 * -------------
 * devenv.exe CodeAnalyzerEx.sln /rebuild release
 *
 * Maintenance History
 * -------------------
 * ver 1.0 : 01 March 2017
 *  - first release
 */

#include <iostream>
#include "../../NoSqlDb/DbEngine/DbMain.h"
#include "../../Parser/ActionsAndRules.h"
#include "..\..\NoSqlDb\Persistence\Persist.h"

namespace CodeAnalysis
{
    class DepAnal
    {
    public:
        //using SPtr = std::shared_ptr<ASTNode*>;
        using FileType = std::string;
        using SqlDbType = std::string;
        using StringDb = NoSqlDb<SqlDbType, SqlDbType>;
        using StringRec = Record<SqlDbType, SqlDbType>;

    private:
        AbstrSynTree& ASTref_;
        ScopeStack<ASTNode*> scopeStack_;
        Scanner::Toker& toker_;
        StringDb& TypeTable_;
        StringDb DepAnalTable;
        std::vector<FileType> Files;

        //Member Function
    private:
        //void generateDepAnalTable(ASTNode* pNode);
        void fillDepTable(ASTNode* pNode);
        void formatDepTable();
        void ParseTokens();
        void DepAnal::addToDepTable(const std::string& Parent, const std::string &Child);

    public:
        DepAnal(NoSqlDb<std::string, std::string>& aTypeTable);
        void doDepAnal();
        void dispalyDepAnal();
        StringDb& getDepTable();
        void setFiles(const std::vector<FileType> files);
        const std::vector<FileType>& getFiles();
        Persist<SqlDbType, SqlDbType>::SPtr GetRootElement();
    };
}