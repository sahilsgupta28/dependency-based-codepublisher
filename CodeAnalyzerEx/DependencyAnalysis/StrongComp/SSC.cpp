/**
 *  Strongly Connected Components
 *  --------------------------------
 *  Builds strongly connected components for dependency analyzer
 *
 *  FileName     : SSC.cpp
 *  Author       : Sahil Gupta
 *  Date         : 01 March 2017
 *  Version      : 1.0
 */

#include <iostream>
#include "SSC.h"
#include "../../NoSqlDb/DbEngine/DbMain.h"

#include "..\TypeAnal\TypeAnal.h"
#include "..\DepAnal\DepAnal.h"
#include "..\..\Analyzer\Executive.h"

StrongConnComp::StrongConnComp(DepAnal::StringDb& aDepTableRef) 
    : DepTableRef(aDepTableRef), 
    ssc(nullptr)
{
}

StrongConnComp::~StrongConnComp()
{
    if (ssc != nullptr)
    {
        delete ssc;
        ssc = nullptr;
    }
}

void StrongConnComp::buildSSC()
{
    ssc = new SSC((int)DepTableRef.GetCount());

    DepAnal::StringDb::Keys MyKeys = DepTableRef.GetKeys();
    for (auto Key : MyKeys)
    {
        auto Childs = DepTableRef.getChild(Key);
        for (auto Child : Childs)
        {
            ssc->addEdge(Key, Child);
        }
    }

    //std::cout << "\nShowing SSC Map\n";
    //ssc->displayMap();
}

void StrongConnComp::doSSC()
{
    buildSSC();
    ssc->doSCC();
}

void StrongConnComp::showSSC()
{
    ssc->showSsc();
}

AbstractXmlElement::sPtr StrongConnComp::GetRootElement()
{
    using Sptr = AbstractXmlElement::sPtr;
    
    Sptr pRoot = makeTaggedElement("StrongComponents");

    SSC::List SccList = ssc->getSscList();
    std::unordered_map<int, std::string>& ReverseMap = ssc->getMap();

    for (auto ci = SccList.begin(); ci != SccList.end(); ++ci)
    {
        Sptr pComponentEle = makeTaggedElement("Component");
        for (vector<int>::const_iterator cv = (*ci).begin(); cv != (*ci).end(); ++cv)
        {
                Sptr pDataElem = makeTaggedElement("File");
                pDataElem->addChild(makeTextElement(ReverseMap[*cv].c_str()));
                pComponentEle->addChild(pDataElem);
        }
        pRoot->addChild(pComponentEle);
    }

    return pRoot;
}


#ifdef TEST_SCC
int main(int argc, char* argv[])
{
    CodeAnalysisExecutive c;
    c.DoCodeAnal(argc, argv);

    TypeAnal ta;
    ta.doTypeAnal();

    DepAnal da(ta.GetTypeTable());
    da.setFiles(c.getAllFiles());
    da.doDepAnal();

    StrongConnComp ssc(da.getDepTable());
    ssc.showSSC();
}
#endif