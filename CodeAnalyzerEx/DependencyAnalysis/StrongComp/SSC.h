#pragma once
/**
 *  Strongly Connected Components
 *  ------------------------------
 *  Builds strongly connected components for dependency analyzer
 *
 *  FileName     : SSC.h
 *  Author       : Sahil Gupta
 *  Date         : 01 March 2017
 *  Version      : 1.0
 *
 *  Package Operations:
 *  -------------
 * Integrates Graph and SCC class with Dependency Analyzer through StrongConnComp class
 * Builds strongly connected component among dependent files in dependency analyzer
 *
 *  Public Interface
 *  ----------------
 * StrongConnComp(DepAnal::StringDb& aDepTableRef);
 * ~StrongConnComp();
 * doSSC();
 * showSSC();
 * GetRootElement();
 * 
 *  Required files
 *  -------------
 *  Graph.h, DepAnal.h, XmlElement.h
 *
 *  Build Process
 *  -------------
 *  devenv.exe CodeAnalyzerEx.sln /rebuild release
 *
 *  Maintenance History
 *  -------------------
 *  ver 1.0 : 01 March 2017
 *   - first release
 */

#include "../Graph/Graph.h"
#include "../DepAnal/DepAnal.h"
#include "../../NoSqlDb/XmlInterface/XmlElement.h"

using namespace CodeAnalysis;
using namespace XmlProcessing;

class StrongConnComp
{
    SSC *ssc;
    DepAnal::StringDb& DepTableRef;

    void buildSSC();
public:
    StrongConnComp(DepAnal::StringDb& aDepTableRef);
    ~StrongConnComp();
    void doSSC();
    void showSSC();
    AbstractXmlElement::sPtr GetRootElement();
};