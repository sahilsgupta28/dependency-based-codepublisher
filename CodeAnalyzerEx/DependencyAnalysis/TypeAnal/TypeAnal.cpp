/**
 * Type Analyzer
 * ---------------------
 * Generate type table by analyzing source files
 *
 * FileName     : TypeAnal.cpp
 * Author       : Sahil Gupta
 * Date         : 01 March 2017
 * Version      : 1.0
 */

#include "TypeAnal.h"
#include "..\..\NoSqlDb\DbEngine\DbMain.h"
#include "..\..\Analyzer\Executive.h"

namespace CodeAnalysis
{

TypeAnal::TypeAnal() :
    ASTref_(Repository::getInstance()->AST()),
    scopeStack_(Repository::getInstance()->scopeStack()),
    toker_(*(Repository::getInstance()->Toker())
    )
{
}

bool doDisplayForTypeTable(ASTNode* pNode)
{
    // namespace - to get global definitions as data declarations
    // class, struct, enum as nodes
    // using as usingdecl in data section of a node
    static std::string toDisplay[] = {
        "namespace", "class", "struct", "enum", "typedef"
    };
    for (std::string type : toDisplay)
    {
        if (pNode->type_ == type)
            return true;
    }
    return false;
}

void TypeAnal::generateTypeTable(ASTNode* pNode)
{
    if (doDisplayForTypeTable(pNode))
    {
        /* look for global declarations, using, typedef */
        for (auto pDecl : pNode->decl_)
        {
            AnalyzeVariableDeclaration(pDecl, pNode->name_);
        }

        if (pNode->type_ != "namespace")
        {
            types.addType(pNode->name_, pNode->type_, pNode->package_);
        }
    }

    //Global Function
    if (pNode->type_ == "namespace")
    {
        for (auto pChild : pNode->children_)
        {
            if (pChild->type_ == "function")
            {
                types.addType(pChild->name_, "global func", pChild->package_);
            }
        }
    }

    for (auto pChild : pNode->children_)
        generateTypeTable(pChild);
}

void TypeAnal::AnalyzeVariableDeclaration(DeclarationNode pDecl, std::string NodeName)
{
    Scanner::SemiExp se;
    se.clone(*pDecl.pTc);

    // Using can come in 2 cases:
    // 1. using namespace X 
    // 2. using X = Y;
    if (pDecl.declType_ == usingDecl)
    {
        // Case 2: using X = Y;
        if (se[2] == "=")
        {
            types.addType(se[1], "using", pDecl.package_);
        }
    }

    // Typedef
    if (se.find("typedef") < se.length())
    {
        types.addType(se[se.find(";") - 1], "typedef", pDecl.package_);
    }

    // Data Declaration Types (Global / Public variable Declaration)
    // 1. Normal        : int g;
    // 2. Initialize    : int g_common = 5;
    // 3. Template      : Convert<int> C;
    // BUG: Some class/struct public declarations are not returned as public.
    if (pDecl.declType_ == dataDecl && (pDecl.access_ == publ || NodeName == "Global Namespace"))
    {
        size_t Index;
        std::string TypeName = (NodeName == "Global Namespace") ? "global var" : "public var";

        // Case 2: Initialize    : int g_common = 5;
        if ((Index = se.find("=")) < se.length())
        {
            types.addType(se[Index - 1], TypeName, pDecl.package_);
        }

        // Case 1,3
        else if ((Index = se.find(";")) < se.length())
        {
            types.addType(se[Index - 1], TypeName, pDecl.package_);
        }
    }
}

void TypeAnal::doTypeAnal()
{
    /*std::cout << "\n  scanning AST and displaying important things:";
    std::cout << "\n -----------------------------------------------";
    DFS(pRoot);*/

    std::cout << "\nStarting Type Analysis...";

    ASTNode* pRoot = ASTref_.root();
    generateTypeTable(pRoot);
}

} //Namespace CodeAnalysis

#ifdef TEST_TYPEANAL
int main(int argc, char* argv[])
{
    CodeAnalysis::CodeAnalysisExecutive c;
    c.DoCodeAnal(argc, argv);

    CodeAnalysis::TypeAnal ta;
    ta.doTypeAnal();
    ta.PersistTypeTable(".//TypeTable.xml");
    return 0;
}
#endif