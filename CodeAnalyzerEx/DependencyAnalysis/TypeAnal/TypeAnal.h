#pragma once
/**
 * Type Analyzer
 * ---------------------
 * Generate type table by analyzing source files
 *
 * FileName     : TypeAnal.h
 * Author       : Sahil Gupta
 * Date         : 01 March 2017
 * Version      : 1.0
 *
 * Package Operations:
 * -------------
 * Finds all the types and global functions defined in each of a collection of C++ source files. 
 * It does this by building rules to detect:
 *   type definitions - classes, structs, enums, typedefs, and aliases.
 *   global function definitions
 *   global data definitions
 *
 * Public Interface
 * ----------------
 * TypeAnal() - create new instance of type analyzer
 * doTypeAnal() - start type analysis
 * GetTypeTable() - return reference to type table
 * PersistTypeTable(std::string FileName) - Persist type table in file
 * GetRootElement() - get reference to XML element to persist
 *
 * Required files
 * -------------
 * TypeTable.h, ActionAndRules.h, AbstrSynTree.h, Persist.h
 *
 * Build Process
 * -------------
 * devenv.exe CodeAnalyzerEx.sln /rebuild release
 *
 * Maintenance History
 * -------------------
 * ver 1.0 : 01 March 2017
 *  - first release
 */

#include <iostream>
#include <functional>
#include "../TypeTable/TypeTable.h"
#include "../../Parser/ActionsAndRules.h"
#include "../../AbstractSyntaxTree/AbstrSynTree.h"
#include "..\..\NoSqlDb\Persistence\Persist.h"

namespace CodeAnalysis
{
    class TypeAnal
    {
        //Data Members
    private:
        AbstrSynTree& ASTref_;
        ScopeStack<ASTNode*> scopeStack_;
        Scanner::Toker& toker_;
        TypeTable types;

        //Member Functions
    private:
        void generateTypeTable(ASTNode* pNode);
        void AnalyzeVariableDeclaration(DeclarationNode pDecl, std::string NodeName);

    public:
        TypeAnal();
        void doTypeAnal();

        TypeTable::StringDb& GetTypeTable() { return types.GetTypeTable(); }
        void PersistTypeTable(std::string FileName) { types.PersistTypeTable(FileName); }
        Persist<TypeTable::SqlDbType, TypeTable::SqlDbType>::SPtr GetRootElement() { return types.GetRootElement(); }
    };
}