#pragma once

/**
 * No SQL Database
 * -----------------
 * FileName     : DbMain.h
 * Author       : Sahil Gupta
 * Date         : 30 January 2017
 * Version      : 1.0
 *
 * Package Operations:
 * -------------------
 * Implements a NoSql database that allows to create, delete, update and query records.
 * Contains following classes:
 * Record  - To identify individual records
 * NoSqlDb - To support database operations like create, delete, update & query.
 *
 * Public Interface
 * ----------------
 * Record<Key,Value>
 * std::string GetName()
 * std::string GetDescription()
 * std::string GetCategory()
 * std::string GetDateTime()
 * std::vector<Key> GetChild()
 * Value GetData()
 * void display()
 *
 * NoSqlDb<Key,Value>
 * -----------STATE--------------
 * bool IsDbModified()
 * void ResetModifiedState()
 * void SetModifiedState()
 *
 * -----------ADD/DELETE------------
 * bool addKey(const Key& key, Value& record)
 * bool deleteKey(const Key& key)
 * bool addChild(const Key& ParentKey, const Key& ChildKey)
 * bool removeChild(const Key& ParentKey, const Key& ChildKey)
 *
 * -----------UPDATE-------------------
 * bool updateName(const Key& key, const std::string Name)
 * bool updateCategory(const Key& key, const std::string Category)
 * bool updateDescription(const Key& key, const std::string Description)
 * bool updateData(const Key& key, const ValueType Data)

 * -----------QUERIES--------
 * const Keys GetKeys() const
 * Value* GetValue(const Key& key)
 * Keys getChild(const Key& key)
 * Keys getKeyNamePattern(const std::string pattern, Keys& keys)
 * Keys getKeyNamePattern(std::regex& pattern, Keys& keys)
 * Keys getNamePattern(const std::string pattern, Keys& keys)
 * Keys getNamePattern(std::regex& pattern, Keys& keys)
 * Keys getCategoryPattern(const std::string pattern, Keys& keys)
 * Keys getCategoryPattern(std::regex& pattern, Keys& keys)
 * Keys getDataPattern(ValueType pattern, Keys& keys)
 * Keys getDataPattern(std::regex& pattern, Keys& keys)
 * Keys getDatePattern(std::string StartDate, std::string EndDate, Keys& keys)
 * size_t GetCount() const
 *
 * -----------DISPLAY------------
 * void displayKeys(const Keys &keys) const
 * void display()
 *
 * Required files
 * -------------
 * convert.h, datetime.h
 *
 * Build Process
 * -------------
 * devenv.exe NoSqlDb.sln /rebuild release
 *
 * Maintenance History
 * -------------------
 * ver 1.0 : 30 January 2017
 *  - first release
 */

#include <iomanip>
#include <iterator>
#include <sstream>
#include <string>
#include <vector>
#include <unordered_map>
#include <atomic>
#include <stdarg.h>
#include <regex>

#include "../Convert/Convert.h"
#include "../DateTimeClass/DateTime.h"

template<typename KeyType, typename ValueType>
class Record
{
    /* ----------------------------------------------------------
                D A T A   M E M B E R S
    ---------------------------------------------------------- */
    template<typename, typename> friend class NoSqlDb;

public:
    using Key = KeyType;

private:
    /* Metadata */
    std::string Name;
    std::string Description;
    std::string Category;
    std::string DateTime;
    std::vector<Key> Child;

    /* Data */
    ValueType Data;

    /* ----------------------------------------------------------
                M E M B E R   F U N C T I O N S
       ---------------------------------------------------------- */
public:
    /* Doubt: Why do we need default constructor? Cant we remove this? */
    Record() = default;

    /* Validate if none of metadata arguments are null
     * This affects load function
     */
    Record(std::string aName, std::string aDsc, std::string aCategory, ValueType aData, std::string aDateTime = "") :
        Name(aName),
        Description(aDsc),
        Category(aCategory),
        Data(aData)
    {
        if (aDateTime.compare("") == 0)
            DateTime = DateTimeClass::GetCurrentDateTime();
        else
            DateTime = aDateTime;
    }

    std::string GetName() { return Name; }
    std::string GetDescription() { return Description; }
    std::string GetCategory() { return Category; }
    std::string GetDateTime() { return DateTime; }
    std::vector<Key> GetChild() { return Child; }
    ValueType GetData() { return Data; }

    void display()
    {
        std::ostringstream out;

        out.setf(std::ios::adjustfield, std::ios::left);

        out << "\n  " << std::setw(11) << "Name" << " : " << Name;
        out << "\n  " << std::setw(11) << "Description" << " : " << Description;
        out << "\n  " << std::setw(11) << "Category" << " : " << Category;
        out << "\n  " << std::setw(11) << "DateTime" << " : " << DateTime;
        for (std::vector<Key>::iterator i = Child.begin(); i != Child.end(); ++i)
        {
            std::string str = Convert<Key>::toString(*i);
            out << "\n    " << std::setw(9) << "Child Key" << " : " << str;
        }
        out << "\n  " << std::setw(11) << "Data" << " : " << Data;
        out << "\n  ";

        std::cout << out.str();
    }

private:
    bool IsChildExist(const Key& childKey)
    {
        if (std::find(Child.begin(), Child.end(), childKey) != Child.end())
            return true;

        return false;
    }

    void addChild(const Key& childKey)
    {
        if (IsChildExist(childKey))
            return;

        Child.push_back(childKey);
    }

    void deleteChild(const Key& childKey)
    {
        std::vector<Key>::const_iterator i;

        i = std::find(Child.begin(), Child.end(), childKey);
        if (i == Child.end())
            return;    //element not found

        Child.erase(i);
    }
};

template<typename KeyType, typename ValueType>
class NoSqlDb
{
    /* ----------------------------------------------------------
                D A T A   M E M B E R S
    ---------------------------------------------------------- */
public:
    using Key = KeyType;
    using Keys = std::vector<Key>;
    using Value = Record<Key, ValueType>;

private:
    using Item = std::pair<Key, Value>;
    std::unordered_map<Key, Value> DataStore;
    std::atomic<bool> IsModified;   /* Used to indicate if there are any changes since last write */
    /* ----------------------------------------------------------
                M E M B E R   F U N C T I O N S
    ---------------------------------------------------------- */
private:
    bool IsKeyExist(const Key& key) const
    {
        if (DataStore.find(key) == DataStore.end())
            return false;

        return true;
    }

public:
    NoSqlDb() : IsModified(false) {}

    bool IsDbModified() const
    {
        return IsModified;
    }

    void ResetModifiedState()
    {
        IsModified = false;
    }

    void SetModifiedState()
    {
        IsModified = true;
    }

    bool addKey(const Key& key, Value& record)
    {
        if (IsKeyExist(key))
            return false;

        //std::cout << "\n[REQUIREMENT 3] Adding key/value pair : " << key;

        DataStore[key] = record;
        SetModifiedState();
        return true;
    }

    bool deleteKey(const Key& key)
    {
        if (!IsKeyExist(key))
            return false;

        /* Remove this key as child if it is for other keys */
        Keys Allkeys = GetKeys();
        for (Key thiskey : Allkeys)
        {
            Value* pVal = GetValue(thiskey);
            pVal->deleteChild(key);
        }

        std::cout << "\n[REQUIREMENT 3] Deleting key/value pair : " << key;

        DataStore.erase(key);
        SetModifiedState();
        return true;
    }

    bool addChild(const Key& ParentKey, const Key& ChildKey)
    {
        /* Check if child key exist in database 
         * Cant do this coz while loading from file, we might have child key whose key has not been loaded yet
         */
       /* Value* pValCh = GetValue(ChildKey);
        if (nullptr == pValCh)
            return false;*/

        /* Check if parent key exist in database and get reference to parent key */
        Value* pVal = GetValue(ParentKey);
        if (nullptr == pVal)
            return false;

        //std::cout << "\n[REQUIREMENT 4 (1)] Add Child Key (" << ChildKey << ") to Key (" << ParentKey << ")";

        pVal->addChild(ChildKey);
        SetModifiedState();
        return true;
    }

    bool removeChild(const Key& ParentKey, const Key& ChildKey)
    {
        Value* pVal = GetValue(ParentKey);
        if (nullptr == pVal)
            return false;

        //std::cout << "\n[REQUIREMENT 4 (1)] Remove Child Key (" << ChildKey << ") from Key (" << ParentKey << ")";

        pVal->deleteChild(ChildKey);
        SetModifiedState();
        return true;
    }

    /* is there a better way to do this? cant keep adding functions for new metadata.. */
    bool updateName(const Key& key, const std::string Name)
    {
        Value* pVal = GetValue(key);
        if (nullptr == pVal)
            return false;
        
        //std::cout << "\n[REQUIREMENT 4 (2)] Update Name to (" << Name << ") for Key (" << key<< ")";

        pVal->Name = Name;
        SetModifiedState();
        return true;
    }

    bool updateCategory(const Key& key, const std::string Category)
    {
        Value* pVal = GetValue(key);
        if (nullptr == pVal)
            return false;

        //std::cout << "\n[REQUIREMENT 4 (2)] Update Category to (" << Category << ") for Key (" << key << ")";

        pVal->Category = Category;
        SetModifiedState();
        return true;
    }

    bool updateDescription(const Key& key, const std::string Description)
    {
        Value* pVal = GetValue(key);
        if (nullptr == pVal)
            return false;

        //std::cout << "\n[REQUIREMENT 4 (3)] Update Description to (" << Description << ") for Key (" << key << ")";

        pVal->Description = Description;
        SetModifiedState();
        return true;
    }

    /* reference? */
    bool updateData(const Key& key, const ValueType Data)
    {
        Value* pVal = GetValue(key);
        if (nullptr == pVal)
            return false;

        //std::cout << "\n[REQUIREMENT 4 (3)] Update Data Instance for Key (" << key << ")";

        pVal->Data = Data;
        SetModifiedState();
        return true;
    }

    /****************************************************************
                            Q U E R I E S
    ****************************************************************/

    /* Return all keys in db */
    const Keys GetKeys() const
    {
        Keys keys;
        for (Item item : DataStore)
            keys.push_back(item.first);

        return keys;
    }

    /* REQUIREMENT 7 (1)
     * Return value of a specified Key
     * todo: make this const?
     */
    Value* GetValue(const Key& key)
    {
        if (!IsKeyExist(key))
            return nullptr;

        return &DataStore[key];
    }

    /* REQUIREMENT 7 (2)
     * Return children of a specified key
     */
    Keys getChild(const Key& key)
    {
        Value* pVal = GetValue(key);
        if (nullptr == pVal)
            return Keys();  //empty vector

        return pVal->Child;
    }

    /* REQUIREMENT 7 (3)
     * The set of all keys matching a specified pattern which defaults to all keys.
     */
    Keys getKeyNamePattern(const std::string pattern, Keys& keys = Keys()) const
    {
        Keys resultKeys;

        /* If user has not given keys, search on all keys */
        if (keys.empty())
            keys = GetKeys();

        for (Key key : keys)
        {
            std::string KeyString = Convert<Key>::toString(key);
            if (KeyString.find(pattern) != std::string::npos)
                resultKeys.push_back(key);
        }

        return resultKeys;
    }

    /* REQUIREMENT 12 [REGEX]
     * The set of all keys matching a specified pattern which defaults to all keys.
     */
    Keys getKeyNamePattern(std::regex& pattern, Keys& keys = Keys())
    {
        Keys resultKeys;

        /* If user has not given keys, search on all keys */
        if (keys.empty())
            keys = GetKeys();

        for (Key key : keys)
        {
            if (regex_match(key, pattern))
                resultKeys.push_back(key);
        }

        return resultKeys;
    }

    /* REQUIREMENT 7 (4)
     * All keys that contain a specified string in their item name
     */
    Keys getNamePattern(const std::string pattern, Keys& keys = Keys())
    {
        Keys resultKeys;

        /* If user has not given keys, search on all keys */
        if (keys.empty())
            keys = GetKeys();

        for (Key key : keys)
        {
            if ((GetValue(key))->Name.find(pattern) != std::string::npos)
                resultKeys.push_back(key);
        }

        return resultKeys;
    }

    /* REQUIREMENT 12 [REGEX] */
    Keys getNamePattern(std::regex& pattern, Keys& keys = Keys())
    {
        Keys resultKeys;

        /* If user has not given keys, search on all keys */
        if (keys.empty())
            keys = GetKeys();

        for (Key key : keys)
        {
            if (regex_match((GetValue(key))->Name, pattern))
                resultKeys.push_back(key);
        }

        return resultKeys;
    }

    /* REQUIREMENT 7 (5)
     * All keys that contain a specified string in their category name
     */
    Keys getCategoryPattern(const std::string pattern, Keys& keys = Keys())
    {
        Keys resultKeys;

        /* If user has not given keys, search on all keys */
        if (keys.empty())
            keys = GetKeys();

        for (Key key : keys)
        {
            if ((GetValue(key))->Category.find(pattern) != std::string::npos)
                resultKeys.push_back(key);
        }

        return resultKeys;
    }

    /* REQUIREMENT 12 [REGEX] */
    Keys getCategoryPattern(std::regex& pattern, Keys& keys = Keys())
    {
        Keys resultKeys;

        /* If user has not given keys, search on all keys */
        if (keys.empty())
            keys = GetKeys();

        for (Key key : keys)
        {
            if (regex_match((GetValue(key))->Category, pattern))
                resultKeys.push_back(key);
        }

        return resultKeys;
    }

    /* REQUIREMENT 7 (6)
     * All keys that contain a specified string in their template data section when that makes sense.
     */
    Keys getDataPattern(ValueType pattern, Keys& keys = Keys())
    {
        Keys resultKeys;

        /* If user has not given keys, search on all keys */
        if (keys.empty())
            keys = GetKeys();

        for (Key key : keys)
        {
            Value* pRec = (GetValue(key));
            std::string DataString = Convert<ValueType>::toString(pRec->Data);
            std::string PatternString = Convert<ValueType>::toString(pattern);
            if (DataString.find(PatternString) != std::string::npos)
                resultKeys.push_back(key);
        }

        return resultKeys;
    }

    /* REQUIREMENT 12 [REGEX] */
    Keys getDataPattern(std::regex& pattern, Keys& keys = Keys())
    {
        Keys resultKeys;

        /* If user has not given keys, search on all keys */
        if (keys.empty())
            keys = GetKeys();

        for (Key key : keys)
        {
            Value* pRec = (GetValue(key));
            std::string DataString = Convert<ValueType>::toString(pRec->Data);
            if (regex_match(DataString, pattern))
                resultKeys.push_back(key);
        }

        return resultKeys;
    }

    /* REQUIREMENT 7 (7)
     * All keys that contain values written within a specified time - date interval.
     * If only one end of the interval is provided shall take the present as the other end of the interval. 
     */
    Keys getDatePattern(std::string StartDate, std::string EndDate = "", Keys& keys = Keys())
    {
        Keys resultKeys;

        /* If user has not given keys, search on all keys */
        if (keys.empty())
            keys = GetKeys();

        if (EndDate.compare(""))
        {
            EndDate = DateTimeClass::GetCurrentDateTime();
        }

        for (Key key : keys)
        {
            Value* pRec = (GetValue(key));
            if (DateTimeClass::IsOlderThan(StartDate, pRec->DateTime) &&
                DateTimeClass::IsOlderThan(pRec->DateTime, EndDate))
            {
                resultKeys.push_back(key);
            }
        }

        return keys;
    }

    size_t GetCount() const
    {
        return DataStore.size();
    }

    void displayKeys(const Keys &keys) const
    {
        for (Key MyKey : keys)
            std::cout << std::endl << MyKey;
        std::cout << std::endl;
    }

    void display()
    {
        const Keys MyKeys = GetKeys();

        for (Key MyKey : MyKeys)
        {
            Value* pVal = GetValue(MyKey);
            if (nullptr == pVal)
            {
                std::cout << "\nInvalid Key (" << MyKey << ")";
            }
            else
            {
                std::string KeyString = Convert<KeyType>::toString(MyKey);
                std::cout << "\n" << KeyString << "\t";
                pVal->display();
            }
        }
    }
};

inline std::ostream& operator << (std::ostream& out, const std::string& str)
{
    try
    {
        out << str.c_str();
    }
    catch (std::exception& ex)
    {
        out << "\nException: " << ex.what() << "\n";
    }

    return out;
}

/* @todo : Use Keys Template type
 * Overload Vector to support union operation
 */
template<typename T>
inline std::vector<T> operator +(const std::vector<T>& lhs, const std::vector<T>& rhs)
{
    std::vector<T> result(lhs);

    for (size_t i = 0; i < rhs.size(); i++)
    {
        if (std::find(result.begin(), result.end(), rhs[i]) == result.end())
            result.push_back(rhs[i]);
    }

    return result;
}

//template<typename K, typename V>
//inline typename NoSqlDb<K, V>::Keys operator+(const typename NoSqlDb<K, V>::Keys& lhs, const typename NoSqlDb<K, V>::Keys& rhs)
//{
//    NoSqlDb<K, V>::Keys result(lhs);
//
//    for (size_t i = 0; i < rhs.size(); i++)
//    {
//        if (std::find(result.begin(), result.end(), rhs[i]) == result.end())
//            result.push_back(rhs[i]);
//    }
//
//    return result;
//}

template<typename T>
inline std::ostream& operator << (std::ostream& os, const std::vector<T>& v)
{
    for (std::vector<T>::const_iterator iter = v.begin(); iter != v.end(); ++iter)
    {
        os << *iter << " ";
    }
    return os;
}

template<typename T>
inline std::istream& operator >> (std::istream& is, std::vector<T>& v)
{
    T data;

    while (is >> data)
    {
        v.push_back(data);
    }

    return is;
}

template<typename T>
inline std::ostream& operator << (std::ostream& os, const std::pair<T, T>& p)
{
    os << "[" << p.first << " ," << p.second << "]";
    return os;
}

template<typename T>
inline std::istream& operator >> (std::istream& is, std::pair<T, T>& p)
{
    is >> p.first;
    is >> p.second;
    return is;
}