/**
* No SQL Database Test
* ---------------------
* Tests functionality of NoSqlDatabase
*
* FileName     : DbMain.cpp
* Author       : Sahil Gupta
* Date         : 30 January 2017
* Version      : 1.0
*/

#include <iostream>
#include <vector>
#include "DbMain.h"

using namespace std;

/* REQUIREMENT 3 */
void CreateStringDb(NoSqlDb<std::string, std::string>& StringDb)
{
    using Record = Record<std::string, std::string>;
    Record R1("Record1", "Sample Record 1", "DataType", "R1_Data");
    Record R2("Record2", "Sample Record 2", "DataType", "R2_Data");
    Record R3("Record3", "Sample Record 3", "DataType", "R3_Data");
    Record R4("Record4", "Sample Record 4", "DataType", "R4_Data");
    Record E1("Element1", "Sample Element 1", "ElementType", "E1_Data");
    Record E2("Element2", "Sample Element 2", "ElementType", "E2_Data");

    StringDb.addKey("Key1", R1);
    StringDb.addKey("Key2", R2);
    StringDb.addKey("Key3", R3);
    StringDb.addKey("Key4", R4);
    StringDb.addKey("El1", E1);
    StringDb.addKey("El2", E2);

    StringDb.deleteKey("Key4");
    StringDb.addKey("Key5", R4);
}

void UpdateStringDb(NoSqlDb<std::string, std::string>& StringDb)
{
    /* REQUIREMENT 4(1) */
    StringDb.addChild("Key1", "Key2");
    StringDb.addChild("Key1", "Key3");
    StringDb.addChild("Key2", "Key3");
    StringDb.removeChild("Key1", "Key2");
    StringDb.addChild("Key2", "Key1");
    StringDb.addChild("El1", "El2");

    /* REQUIREMENT 4(2) */
    StringDb.updateName("Key1", "Record1 Updated");
    StringDb.updateCategory("Key1", "UpdatedDataType");
    StringDb.updateDescription("Key3", "Updated Sample Record 3");

    /* REQUIREMENT 4(3) */
    StringDb.updateData("Key1", "Updated Data");

    std::cout << "\n[REQUIREMENT 4] Displaying in-memory database after updations";
    StringDb.display();

}

void QueryStringDb(NoSqlDb<std::string,std::string>& StringDb)
{
    NoSqlDb<std::string, std::string>::Keys MyKeys;
    using Record = Record<std::string, std::string>;

    /* REQUIREMENT 7 (1) */
    std::cout << "\n[REQUIREMENT 7(1)] Value for Key \"Key1\"";
    Record* pRec = StringDb.GetValue("Key1");
    pRec->display();

    /* REQUIREMENT 7 (2) */
    std::cout << "\n[REQUIREMENT 7(2)] Children of Key \"Key2\"";
    MyKeys = StringDb.getChild("Key2");
    StringDb.displayKeys(MyKeys);
    //MyKeys.clear();

    /* REQUIREMENT 7 (3) */
    std::cout << "\n[REQUIREMENT 7(3)] Key Pattern Matching \"1\" from children of key \"Key2\"";
    MyKeys = StringDb.getKeyNamePattern("1", MyKeys);
    StringDb.displayKeys(MyKeys);
    MyKeys.clear();

    /* REQUIREMENT 7 (4) */
    std::cout << "\n[REQUIREMENT 7(4)] Category Pattern Matching \"Data\"";
    MyKeys = StringDb.getCategoryPattern("Data");
    StringDb.displayKeys(MyKeys);
    //MyKeys.clear();

    /* REQUIREMENT 7 (5) */
    std::cout << "\n[REQUIREMENT 7(5)] Name Pattern Matching \"1\" from Category Pattern Matching \"Data\"";
    MyKeys = StringDb.getNamePattern("1", MyKeys);
    StringDb.displayKeys(MyKeys);
    MyKeys.clear();

    /* REQUIREMENT 7 (6) */
    std::cout << "\n[REQUIREMENT 7(6)] Data Pattern Matching \"2\"";
    MyKeys = StringDb.getDataPattern("2");
    StringDb.displayKeys(MyKeys);
    MyKeys.clear();

    /* REQUIREMENT 7 (7) - "YYYY/DD/MM HH:MM:SS" */
    std::cout << "\n[REQUIREMENT 7(7)] Date";
    MyKeys = StringDb.getDatePattern("2017/08/01 00:32:03", "2017/09/01 00:32:03");
    StringDb.displayKeys(MyKeys);
    MyKeys.clear();
}

void QueryRegExStringDb(NoSqlDb<std::string, std::string>& StringDb)
{
    NoSqlDb<std::string, std::string>::Keys MyKeys;
    using Record = Record<std::string, std::string>;
 
    std::cout << "\n[REQUIREMENT 12] RegEx Keys \"Key[4 - 7]\"";
    MyKeys = StringDb.getKeyNamePattern(std::regex("Key[4-7]"));
    StringDb.displayKeys(MyKeys);
    MyKeys.clear();

    std::cout << "\n[REQUIREMENT 12] RegEx Name \"[[:alpha:]]+2\"";
    MyKeys = StringDb.getNamePattern(std::regex("[[:alpha:]]+2"));
    StringDb.displayKeys(MyKeys);
    MyKeys.clear();

    std::cout << "\n[REQUIREMENT 12] RegEx Category \"Data[[:alpha:]]+\"";
    MyKeys = StringDb.getCategoryPattern(std::regex("Data[[:alpha:]]+"));
    StringDb.displayKeys(MyKeys);
    MyKeys.clear();

    std::cout << "\n[REQUIREMENT 12] RegEx Data \"R[[:digit:]]+_Data\"";
    MyKeys = StringDb.getDataPattern(std::regex("R[[:digit:]]+_Data"));
    StringDb.displayKeys(MyKeys);
    MyKeys.clear();
}

void TestStringDatabase()
{
    NoSqlDb<std::string, std::string> StringDb;

    std::cout << "\n[REQUIREMENT 3] Add/Delete key/value pairs";
    CreateStringDb(StringDb); /* REQUIREMENT 3 */

    std::cout << "\n[REQUIREMENT 2] Displaying in-memory database";
    StringDb.display(); /* REQUIREMENT 2 */

    std::cout << "\n[REQUIREMENT 4] Update Database";
    UpdateStringDb(StringDb); /* REQUIREMENT 4 */

    std::cout << "\n[REQUIREMENT 7] Database Queries";
    QueryStringDb(StringDb); /* REQUIREMENT 7 */

    NoSqlDb<std::string, std::string>::Keys MyKeys;

    /* REQUIREMENT 8 */
    std::cout << "\n[REQUIREMENT 8] Category Pattern Matching \"ElementType\" from Name Pattern Matching \"2\"";
    MyKeys = StringDb.getCategoryPattern("ElementType", StringDb.getNamePattern("2"));
    StringDb.displayKeys(MyKeys);
    MyKeys.clear();

    /* REQUIREMENT 9 */
    std::cout << "\n[REQUIREMENT 9] Union of CategoryPattern(ElementType) + NamePattern(2) + KeyPattern(Key3)";
    MyKeys = StringDb.getCategoryPattern("ElementType") + StringDb.getNamePattern("2") + StringDb.getKeyNamePattern("Key3");
    StringDb.displayKeys(MyKeys);
    MyKeys.clear();

    std::cout << "\n[REQUIREMENT 12] Database Regular Expression Queries";
    QueryRegExStringDb(StringDb); /* REQUIREMENT 12 */

    std::cout << std::endl;
}

void TestVectorDatabase()
{
    NoSqlDb<std::string, std::vector<int>> IntVectorDb;

    using Record = Record<std::string, std::vector<int>>;
    Record R1("Record1", "Sample Record 1", "DataType", { 1, 2, 3 });
    Record R2("Record2", "Sample Record 2", "DataType", { 4, 5, 6 });

    IntVectorDb.addKey("Key1", R1);
    IntVectorDb.addKey("Key2", R2);
    IntVectorDb.addChild("Key1", "Key2");
    IntVectorDb.updateData("Key1", { 7, 8, 9 });

    cout << "\n[REQUIREMENT 2] Template Vector<Int> Database";
    IntVectorDb.display();

    NoSqlDb<std::string, std::string>::Keys MyKeys;

    std::cout << "\n[REQUIREMENT 7(6)] Data Pattern Matching \"4\"";
    MyKeys = IntVectorDb.getDataPattern({ 4 });
    IntVectorDb.displayKeys(MyKeys);
    MyKeys.clear();

    std::cout << std::endl;
}

void TestIntegerKeyDatabase()
{
    NoSqlDb<int, int> StringDb;

    Record<int, int> R1("Record 1", "Sample Description 1", "Test", 1);
    Record<int, int> R2("Record 2", "Sample Description 2", "Test", 2);
    Record<int, int> R3("Record 3", "Sample Description 3", "Test", 3);

    StringDb.addKey(1, R1);
    StringDb.addKey(2, R2);
    StringDb.addKey(3, R3);

    StringDb.addChild(1, 2);
    StringDb.addChild(1, 3);
    StringDb.addChild(2, 3);

    std::cout << "Size of Database = " << StringDb.GetCount() << std::endl;
    StringDb.display();

    StringDb.deleteKey(3);

    std::cout << std::endl << "Size of Database = " << StringDb.GetCount() << std::endl;
    StringDb.display();
    std::cout << std::endl;
}

void CreateProjectStructureDb()
{
    NoSqlDb<std::string, vector<std::string>> PrjStDb;

    Record<std::string, vector<std::string>> Convert(
        "Convert Package",  "Serializes data to string type", "Utility", 
        {"Convert.cpp", "Convert.h"});
    Record<std::string, vector<std::string>> DateTimeClass(
        "DateTime Package", "Represents an instant in time", "Utility",
        { "DateTime.cpp", "DateTime.h" });
    Record<std::string, vector<std::string>> DbEngine(
        "NoSqlDb Package", "No SQL Database", "Main",
        { "DbMain.cpp", "DbMain.h" });
    Record<std::string, vector<std::string>> Persistence(
        "Persist Package", "Provide Persistence for NoSql Database", "Main",
        { "Persist.cpp", "Persist.h" });
    Record<std::string, vector<std::string>> XMLInterface(
        "XML Package", "Provide XML Interfacing Functionality", "Utiity",
        { "itokcollection.h", "Tokenizer.h", "XmlDocument.h", "XmlElement.h", "xmlElementParts.h", "XmlParser.h",
          "Tokenizer.cpp", "XmlDocument.cpp", "XmlElement.cpp", "xmlElementParts.cpp", "XmlParser.cpp"});

    PrjStDb.addKey("Convert", Convert);
    PrjStDb.addKey("DateTime", DateTimeClass);
    PrjStDb.addKey("DbEngine", DbEngine);
    PrjStDb.addKey("Persistence", Persistence);
    PrjStDb.addKey("XmlInterface", XMLInterface);

    PrjStDb.addChild("DbEngine", "Convert");
    PrjStDb.addChild("DbEngine", "DateTime");
    PrjStDb.addChild("DbEngine", "XmlInterface");
    PrjStDb.addChild("Persistence", "DbEngine");
    PrjStDb.addChild("Persistence", "XmlInterface");
    PrjStDb.addChild("Persistence", "Convert");
    PrjStDb.addChild("Persistence", "DateTime");

    std::cout << "\n[REQUIREMENT 10] Loaded Project Structure";
    PrjStDb.display();

    std::cout << std::endl;
}

#ifdef __TEST_NOSQL__
int main()
{
    TestStringDatabase();

    TestVectorDatabase();
    
    //TestIntegerKeyDatabase();

    CreateProjectStructureDb();
}
#endif