/**
 * Persist NoSqlDatabase Test
 * ---------------------
 * Tests Persistance of Database
 *
 * FileName     : Persist.cpp
 * Author       : Sahil Gupta
 * Date         : 7 February 2017
 * Version      : 1.0
 */

#include <iostream>
#include <thread>
#include <chrono>
#include <stdlib.h>
#include "Persist.h"

#ifdef _TEST_PERSIST

int main()
{
    NoSqlDb<std::string, std::string> StringDb;
    Persist<std::string, std::string> PersistStringDb(StringDb, true, "StringDb.xml");

    PersistStringDb.load();

    using Record = Record<std::string, std::string>;
    Record R1("Record1", "Sample Description 1", "Test", "R1 Data");
    Record R2("Record2", "Sample Description 2", "Test", "R2 Data");
    Record R3("Record3", "Sample Description 3", "Test", "R3 Data");

    StringDb.addKey("Key1", R1);
    StringDb.addKey("Key2", R2);
    StringDb.addKey("Key3", R3);
    StringDb.addChild("Key1", "Key2");
    StringDb.addChild("Key1", "Key3");
    StringDb.addChild("Key2", "Key3");

    StringDb.display();
    PersistStringDb.save();

    NoSqlDb<std::string, std::string> StringDb2;
    Persist<std::string, std::string> PersistStringDb2(StringDb2);

    PersistStringDb2.StartTimedPersistence(std::chrono::seconds(2));

    PersistStringDb2.load();
    std::this_thread::sleep_for(std::chrono::seconds(5));
    PersistStringDb2.StopTimedPersistence();

    StringDb2.display();
    std::cout << std::endl;
}

#endif //_TEST_PERSIST