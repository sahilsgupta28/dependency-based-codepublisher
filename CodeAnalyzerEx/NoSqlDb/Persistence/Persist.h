#pragma once

/**
 * Persist NoSqlDatabase
 * -----------------
 * FileName     : Persist.h
 * Author       : Sahil Gupta
 * Date         : 7 February 2017
 * Version      : 1.0
 *
 * Package Operations:
 * -------------------
 * Adds persistance to NoSql Database
 *
 * XML File Structure
 *  <NoSqlDb>
 *      <Record>
 *          <Key>K1</Key>
 *          <Name>R1</Name>
 *          <Category>C1</Category>
 *          <Description>D1</Description>
 *          <TimeDate></TimeDate>
 *          <Child>K1</Child>
 *          <Data>Data1</Data>
 *      </Record>
 *      <Record>
 *          ...
 *      </Record>
 *  </NoSqlDb>
 *
 * Public Interface
 * -------------
 * Persist(NoSqlDb<KeyType, ValueType>& db, string XmlPath)
 * ~Persist()
 * void save()
 * Xml DbToString()
 * void load()
 * void TimedPersistence()
 * void StartTimedPersistence(chrono::seconds Wttime)
 * void StopTimedPersistence()
 * void Wait()
 *
 * Required files
 * -------------
 * DbMain.h, DateTime.h, Convert.h,
 * XmlDocument.h, XmlElement.h
 *
 * Build Process
 * -------------
 * devenv.exe NoSqlDb.sln /rebuild release
 *
 * Maintenance History
 * -------------------
 * ver 1.0 : 30 January 2017
 *  - first release
 */

#include <fstream>
#include <string>
#include <iostream>
#include <chrono>
#include <thread>
#include <atomic>

#include "../DbEngine/DbMain.h"
#include "../XmlInterface/XmlDocument.h"
#include "../XmlInterface/XmlElement.h"
#include "../DateTimeClass/DateTime.h"
#include "../Convert/Convert.h"

using namespace XmlProcessing;
using namespace std;

template<typename KeyType, typename ValueType>
class Persist
{
public:
    using Xml = string;
    using SPtr = shared_ptr<AbstractXmlElement>;
private:
    NoSqlDb<KeyType,ValueType>& DataInstance_;
    string FilePath;
    chrono::seconds WaitTime;
    atomic<bool> IsTimedPersistence;
    atomic<bool> TerminateThread;
    thread ThTimedPersistence;
    bool bAutoSave;
public:
    Persist(NoSqlDb<KeyType, ValueType>& db, bool abAutoSave = false, string XmlPath = "db.xml");
    ~Persist();
    void save();
    Xml DbToString();
    SPtr GetXMLRoot(const std::string RootName);
    void load();
    void TimedPersistence();
    void StartTimedPersistence(chrono::seconds Wttime = chrono::seconds(2));
    void StopTimedPersistence();
    void Wait();

private:
    SPtr RecordToXml(typename NoSqlDb<KeyType, ValueType>::Key KeyInstance);
};

template<typename K, typename V>
Persist<K, V>::Persist(
    NoSqlDb<K, V>& db,
    bool abAutoSave = false,
    string XmlPath = "db.xml") :
        DataInstance_(db),
        FilePath(XmlPath),
        bAutoSave(abAutoSave),
        WaitTime(chrono::seconds(2)),
        IsTimedPersistence(false),
        TerminateThread(false),
        ThTimedPersistence([this]() {TimedPersistence(); })
{
}

template<typename K, typename V>
Persist<K, V>::~Persist()
{
    /* Set flag to terminate thread for persistence */
    Wait();
    
    /* If database is modified then save the modifications on disk */
    if (bAutoSave && DataInstance_.IsDbModified())
    {
        cout << "Persisting Database.";
        save();
    }
}

template<typename K, typename V>
void Persist<K, V>::save()
{
    //cout << "\n[REQUIREMENT 5] Persisting database to file " << FilePath;
    ofstream out(FilePath);
    out << DbToString();
    out.close();
    DataInstance_.ResetModifiedState();
}

template<typename K, typename V>
typename Persist<K, V>::Xml Persist<K,V>::DbToString()
{
    XmlDocument doc;

    doc.docElement() = GetXMLRoot("NoSqlDb");

    return doc.toString();
}

template<typename K, typename V>
typename Persist<K, V>::SPtr Persist<K, V>::GetXMLRoot(const std::string RootName)
{
    /* Make Root Element */
    SPtr pRoot = makeTaggedElement(RootName);

    /* For each record in database, add new entry in XML */
    const typename NoSqlDb<K, V>::Keys MyKeys = DataInstance_.GetKeys();
    for (typename NoSqlDb<K, V>::Key MyKey : MyKeys)
    {
        SPtr pRecElem = RecordToXml(MyKey);
        pRoot->addChild(pRecElem);
    }

    return pRoot;
}

template<typename K, typename V>
typename Persist<K, V>::SPtr Persist<K, V>::RecordToXml(typename NoSqlDb<K, V>::Key KeyInstance)
{
    SPtr pRecElem = makeTaggedElement("Record");

    SPtr pKeyElem = makeTaggedElement("Key");
    string KeyStr = Convert<K>::toString(KeyInstance);
    size_t pos; /* Handle < as they are special chars for XML */
    while ((pos = KeyStr.find_first_of("<")) != std::string::npos) {
        KeyStr.erase(pos, 1);
        KeyStr.insert(pos, "&lt;");
    }

    pKeyElem->addChild(makeTextElement(KeyStr));
    pRecElem->addChild(pKeyElem);

    Record<K, V> *pRec = DataInstance_.GetValue(KeyInstance);

    SPtr pNameElem = makeTaggedElement("Name");
    pNameElem->addChild(makeTextElement(pRec->GetName()));
    pRecElem->addChild(pNameElem);

    SPtr pDescElem = makeTaggedElement("Description");
    pDescElem->addChild(makeTextElement(pRec->GetDescription()));
    pRecElem->addChild(pDescElem);

    SPtr pCateElem = makeTaggedElement("Category");
    pCateElem->addChild(makeTextElement(pRec->GetCategory()));
    pRecElem->addChild(pCateElem);

    SPtr pDTElem = makeTaggedElement("DateTime");
    pDTElem->addChild(makeTextElement(pRec->GetDateTime()));
    pRecElem->addChild(pDTElem);

    vector<K> Child = pRec->GetChild();
    for (vector<K>::const_iterator i = Child.begin(); i != Child.end(); ++i) {
        SPtr pChildElem = makeTaggedElement("Child");
        pChildElem->addChild(makeTextElement(*i));
        pRecElem->addChild(pChildElem);
    }

    SPtr pDataElem = makeTaggedElement("Data");
    string DataStr = Convert<V>::toString(pRec->GetData());
    pDataElem->addChild(makeTextElement(DataStr));
    pRecElem->addChild(pDataElem);

    return pRecElem;
}

/*
 *          <Key>K1</Key>
 *          <Name>R1</Name>
 *          <Category>C1</Category>
 *          <Description>D1</Description>
 *          <TimeDate></TimeDate>
 *          <Child>K1</Child>
 *          <Data>Data1</Data>
 */
 template<typename K, typename V>
 void Persist<K, V>::load()
 {
     try {
         /* Get full file path and display to user */
         cout << "\n[REQUIREMENT 5] Restoring database from file " << FilePath;
         char full[_MAX_PATH];
         if (_fullpath(full, FilePath.c_str(), _MAX_PATH) != NULL)
             cout << "\nLocation : " << full;

         XmlDocument doc(FilePath, XmlDocument::sourceType::file);
         //cout << doc.toString() << endl;

         /* For each record in XML, create new instance of record and add to NoSql database */
         vector<SPtr> pRecords = doc.element("Record").select();
         for (SPtr pRecord : pRecords)
         {
             vector<SPtr> pRecElems = pRecord->children();
             string Key = pRecElems[0]->children()[0]->value();
             string Name = pRecElems[1]->children()[0]->value();
             string Description = pRecElems[2]->children()[0]->value();
             string Category = pRecElems[3]->children()[0]->value();
             string TimeDate = pRecElems[4]->children()[0]->value();
             string DataString = pRecElems[pRecElems.size() - 1]->children()[0]->value();
             V Data = Convert<V>::fromString(DataString);

             Record<K, V> R(trim(Name), trim(Description), trim(Category), Data, trim(TimeDate));
             DataInstance_.addKey(trim(Key), R);

             for (size_t i = 0; i < pRecElems.size() - 6; i++)
             {
                 string Child = pRecElems[5 + i]->children()[0]->value();
                 DataInstance_.addChild(trim(Key), trim(Child));
             }
         }
     } catch (exception& Ex) {
         cout << "\nError : " << Ex.what();
         cout << "\nNew file will be created to persist database";
     }
}

template<typename K, typename V>
void Persist<K, V>::TimedPersistence()
{
    while (true)
    {
        /* Sleep for time specified by user */
        this_thread::sleep_for(WaitTime);

        /* Destructor sets flag to terminate thread */
        if (TerminateThread)
            break;

        /* Save database is timed persistence is turned on and there are changes in database */
        if (IsTimedPersistence)
        {
            if (DataInstance_.IsDbModified())
            {
                cout << "\nPersisting Database at " << DateTimeClass::GetCurrentDateTime();
                save();
            }
            else
            {
                cout << "\nNo Changes in Database at " << DateTimeClass::GetCurrentDateTime();
            }
        }
    }
}

template<typename K, typename V>
void Persist<K, V>::StartTimedPersistence(chrono::seconds Wttime = chrono::seconds(2))
{
    cout << "\n[REQUIREMENT 6] Starting Timed Persistence with wait time " << std::chrono::seconds(Wttime).count() << endl;
    WaitTime = Wttime;
    IsTimedPersistence = true;
}

template<typename K, typename V>
void Persist<K, V>::StopTimedPersistence()
{
    cout << "\n[REQUIREMENT 6] Stopping Timed Persistence.";
    IsTimedPersistence = false;
}

template<typename K, typename V>
void Persist<K, V>::Wait()
{
    if (!IsTimedPersistence)
        TerminateThread = true;
    ThTimedPersistence.join();
}
