/**
 * DateTime Test
 * ---------------------
 * Tests functionality of DateTime Class
 *
 * FileName     : DateTime.cpp
 * Author       : Sahil Gupta
 * Date         : 7 February 2017
 * Version      : 1.0
 */

#include <iostream>
#include <string>
#include <ctime>

#include "DateTime.h"

std::string DateTimeClass::GetCurrentDateTime()
{
    //char ctime[32];
    //struct tm newtime;

    //time_t now = time(0);
    //localtime_s(&newtime, &now);
    //asctime_s(ctime, 32, &newtime);
    //std::string DT(ctime);
    //return DT.substr(0, DT.size() - 1);

    time_t t = time(nullptr);
    tm tm;
    localtime_s(&tm, &t);
    std::stringstream ss;
    ss << std::put_time(&tm, "%Y/%d/%m %H:%M:%S");
    return ss.str();
}

/* Enter date in "YYYY/DD/MM HH:MM:SS" in 24-hour format
* Return true if date specified by Value comes after reference date
*/
bool DateTimeClass::IsOlderThan(const std::string& Reference, const std::string& Value)
{
    if (Reference.compare(Value) >= 0)
        return false;

    return true;
}

#ifdef __DATE__TIME__

int main()
{
    std::cout << DateTimeClass::GetCurrentDateTime() << std::endl;

    std::string D1 = "2017/08/02 00:18:21";
    std::string D2 = "2017/07/02 00:18:21";
    std::cout << DateTimeClass::IsOlderThan(D1, D2) << std::endl;
}

#endif // __DATE__TIME__