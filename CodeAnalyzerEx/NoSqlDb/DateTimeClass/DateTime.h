#pragma once

/**
 * DateTime
 * -----------------
 * FileName     : DateTime.h
 * Author       : Sahil Gupta
 * Date         : 7 February 2017
 * Version      : 1.0
 *
 * Public Interface
 * -------------
 * static std::string GetCurrentDateTime()
 * static bool IsOlderThan(const std::string& Reference, const std::string& Value)
 *
 * Required files
 * -------------
 * None
 *
 * Maintenance History
 * -------------------
 * ver 1.0 : 30 January 2017
 *  - first release
 */

#include <string>
#include <sstream>
#include <iomanip>

class DateTimeClass
{
public:
    static std::string GetCurrentDateTime();
    static bool IsOlderThan(const std::string& Reference, const std::string& Value);
};