#include <iostream>
#include "Convert.h"
#include "DateTime.h"

using namespace CConv;

int g_Executive = 5;

int main()
{
    DateTimeStruct dt;
    dt.setFormat(DateTimeStruct::DT1_FORMAT::MILITARY);

    std::cout << DateTimeStruct::GetCurrentDateTime() << std::endl;

    std::string D1 = "2017/08/02 00:18:21";
    std::string D2 = "2017/07/02 00:18:21";
    std::cout << DateTimeStruct::IsOlderThan(D1, D2) << std::endl;

    std::string intString = Convert<int>::toString(42);
    std::cout << "\n  conversion of integer: " << intString;
    std::cout << "\n  retrieving integer:    " << Convert<int>::fromString(intString);
    std::cout << std::endl;
}