#pragma once
/////////////////////////////////////////////////////////////////////
// Convert.h - Implement serialization to and from strings         //
//                                                                 //
// Jim Fawcett, CSE687 - Object Oriented Design, Spring 2017       //
/////////////////////////////////////////////////////////////////////

/**
 * Convert
 * -----------------
 * FileName     : Convert.h
 * Author       : Jim Fawcett
 * Author       : Sahil Gupta
 * Date         : 7 February 2017
 * Version      : 1.0
 *
 * Public Interface
 * -------------
 * virtual ~Convert<T>()
 * static std::string toString(const T& t)
 * static T fromString(const std::string& str)
 * static std::string id(const T& t)
 *
 * Required files
 * -------------
 * None
 *
 * Maintenance History
 * -------------------
 * ver 1.0 : 7 February 2017
 *  - first release
 */


#include <sstream>
#include <string>
#include <algorithm>
#include <cctype>
#include <functional> 

#include "common.h"

/////////////////////////////////////////////////////////////////////
// Convert<T> class
// - (de)serializes objects of type T (from)to strings
// - generates a type identifier using Run-Time Type Identification
//
namespace CConv {
    template<typename T>
    class Convert
    {
    public:
        using CType = std::string;
        int Conv_publ_int;

        virtual ~Convert<T>() {}
        static CType toString(const T& t);
        static T fromString(const CType& str);
        static CType id(const T& t);
    };

    /*----< serialize t to a std::string >-----------------------------*/
    /*
        * Assumes that T defines an insertion operator.
        */
    template<typename T>
    typename Convert<T>::CType Convert<T>::toString(const T& t)
    {
        std::ostringstream out;
        out << *const_cast<T*>(&t);
        return out.str();
    }

    /*----< create an instance of T from a std::string >---------------*/
    /*
        * Assumes that T defines an extraction operator.
        * To succeed str must be created from Convert<T>::toString(const T& t).
        * @bug : Concatenates vector of string without spaces... Need Specialization?
        */
    template<typename T>
    T Convert<T>::fromString(const typename Convert<T>::CType& str)
    {
        T valueTmp;
        //T valueSum;
        std::istringstream in(str);

        //while (in >> valueTmp);
            //valueSum = valueSum + valueTmp;
        in >> valueTmp;    // istringstream extraction provides the conversion

        //return valueSum;
        return valueTmp;
    }
    /*----< create an id string from an instance of T using RTTI >----*/

    template<typename T>
    typename Convert<T>::CType Convert<T>::id(const T& t)
    {
        Conv_publ_int = g_common_DTC;
        return typeid(t).name();
    }

    // trim from start
    static inline std::string &ltrim(std::string &s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(),
            std::not1(std::ptr_fun<int, int>(std::isspace))));
        return s;
    }

    // trim from end
    static inline std::string &rtrim(std::string &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(),
            std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
        return s;
    }

    // trim from both ends
    static inline std::string &trim(std::string &s) {
        try { return ltrim(rtrim(s)); }
        catch (...) { return s; }
    }
}